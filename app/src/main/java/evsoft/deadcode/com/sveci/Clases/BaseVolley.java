package evsoft.deadcode.com.sveci.Clases;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;

public class BaseVolley {
    public static final int INITIAL_TIMEOUT_MS = 2000;
    public static final int MAX_NUM_RETRIES = 3;
    private Volleys volley;
    protected RequestQueue fRequestQueue;
    private Context context;

    public BaseVolley(Context context) {
               this.context = context;

               volley=Volleys.getInstance(context.getApplicationContext());
               fRequestQueue=volley.getRequestQueue();

    }

    public void onStop(){
        if(fRequestQueue!=null){
            fRequestQueue.cancelAll(this);
        }
    }

    public void  addToQueue(Request request){

        if( request!=null){
            request.setTag(this);

            if(fRequestQueue==null){
                fRequestQueue=volley.getRequestQueue();
            }
            request.setRetryPolicy(new DefaultRetryPolicy(
                    INITIAL_TIMEOUT_MS,
                    MAX_NUM_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            fRequestQueue.add(request);
        }

    }
}
