package evsoft.deadcode.com.sveci;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class configuracionActivity extends AppCompatActivity {



    private TextView url;
    private Button btn_guardar;
    private Button btn_cancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        showToolbar("Configuraciones", true);

        btn_guardar= (Button) findViewById(R.id.btn_guardarconfiguracion);
        btn_cancelar= (Button) findViewById(R.id.btn_regresarconfiguracion);
        url=(TextView) findViewById(R.id.txt_urlservidor);
        cargarValores();

        initiListeners();


    }

    private void cargarValores(){

        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

//        final String codigo = sharedPreferences.getString("codigo", "");
        final String UrlApi=sharedPreferences.getString("url","");

        if(!UrlApi.isEmpty()){

            url.setText(UrlApi);

        }
        else{
            url.setText(R.string.ApiPath);
        }
    }

    public void showToolbar(String titulo, boolean upButton) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

    }

    private void initiListeners() {


        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarConfiguracion();
            }

        });

        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent_menu = new Intent(getApplicationContext(),MainActivity.class);
////                startActivity(intent_menu);
////                finish();
                onBackPressed();

            }
        });


    }

    private void guardarConfiguracion(){

        if(url.getText()!=""){
           boolean validate=Patterns.WEB_URL.matcher(url.getText()).matches();
           if(validate){
               SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url",url.getText().toString());
            editor.commit();
               Toast.makeText(this,"Las configuraciones se guardaron con exito",Toast.LENGTH_SHORT).show();

           }
           else{
               Toast.makeText(this,"Introduzca una url valida",Toast.LENGTH_SHORT).show();
           }


        }
        else{
            Toast.makeText(this,"Introduzca una url ",Toast.LENGTH_SHORT).show();

        }


    }
}
