package evsoft.deadcode.com.sveci;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONObject;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.GPSTracker;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteBebe;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteMujer;


public class NotificarMuerteBebeActivity extends AppCompatActivity {


    public static final int REQUEST_CODE = 1;
    private Button btn_notificar;
    private FloatingActionButton btn_buscar;
    private Toolbar toolbar;

    private TextInputEditText txt_nombres;
    private TextInputEditText txt_apellidos;
    private TextInputEditText txt_direccion;
    private Mujer mujer;
    private Networks network;
    private Button btn_limpiar;


    private RadioButton rb_nacidovivo;
    private RadioButton rb_nacidomuerto;
    private RadioButton rb_noconozco;
//    private Switch aSwitch;
    private int nacido=1;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    private Boolean isActiveGps=false;
    private GPSTracker gpsTracker;
    private Location location;
    private DataConnection webservice;
    private Database database;
    private RadioGroup radio_group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificar_muerte_bebe);

        initActivity();
        showToolbar("NOTIFICAR MUERTE DE BEBE",true);
        initiListeners();

        gpsTracker=new GPSTracker(getApplicationContext(),this);

        if (!gpsTracker.checkPermissions()) {
            requestPermissions();
        } else {
            this.location=gpsTracker.getLocation();
        }


       }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }
    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {

            startLocationPermissionRequest();


        } else {

            startLocationPermissionRequest();
        }
    }


    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
    }
    private void deshabilitarCampos(){
        txt_nombres.setEnabled(false);
        txt_apellidos.setEnabled(false);

    }
    private void initActivity() {

        btn_notificar=(Button) findViewById(R.id.btn_notificar_muerte_bebe);
        txt_nombres=(TextInputEditText)findViewById(R.id.txt_notiMuerteBebeNombre);
        txt_apellidos=(TextInputEditText)findViewById(R.id.txt_notiMuerteBebeApellidos);
        txt_direccion=(TextInputEditText)findViewById(R.id.txt_notiMuerteBebeDireccion);
        rb_nacidovivo=(RadioButton) findViewById(R.id.rb_notiNacidoVivo);
        rb_nacidomuerto=(RadioButton) findViewById(R.id.rb_notiNacidoMuerto);
        rb_noconozco=(RadioButton) findViewById(R.id.rb_notiNacidoNoConozco);
        radio_group=(RadioGroup) findViewById(R.id.rb_group_muertebebe);

        database=new Database(this);


        btn_buscar=(FloatingActionButton) findViewById(R.id.fab_buscar_muerte_bebe);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.purple);


        mujer=null;

        btn_limpiar=(Button) findViewById(R.id.btn_notiMuerteBebeLimpiar);

        this.network=new Networks(this);
        deshabilitarCampos();

    }

    private void initiListeners() {


        btn_notificar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                notificar();
            }

        });

        btn_buscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                buscarMujer();
            }

        });

        btn_limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarVista();
            }
        });

        rb_nacidovivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_nacidovivo.isChecked()){

                    nacido=1;
                }

            }
        });
        rb_nacidomuerto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_nacidomuerto.isChecked()){

                    nacido=1;
                }

            }
        });
        rb_noconozco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_noconozco.isChecked()){

                    nacido=0;
                }

            }
        });


    }

    private void buscarMujer(){

        Intent intent=new Intent(this, ListaMujeresActivity.class);
        intent.putExtra("retroceder",true);

        startActivityForResult(intent,REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(REQUEST_CODE==requestCode && resultCode==RESULT_OK ){
            if( data.getExtras()!=null){

                mujer=(Mujer) data.getExtras().getSerializable("mujer");
                btn_limpiar.setVisibility(View.VISIBLE);
                llenarformulario(mujer);

            }

        }
    }

    public void  llenarformulario(Mujer mujer){


        txt_nombres.setText(mujer.getNombres());
        txt_nombres.setEnabled(false);

        txt_apellidos.setText(mujer.getApellidopaterno() +" "+mujer.getApellidomaterno());
        txt_apellidos.setEnabled(false);
        txt_direccion.setText(mujer.getDireccion());
        txt_direccion.setEnabled(false);

    }

    public void limpiarVista(){

        mujer=null;
        txt_nombres.setText("");

        txt_apellidos.setText("");
        txt_direccion.setText("");
//        txt_nombres.setEnabled(true);
//        txt_apellidos.setEnabled(true);
//        txt_direccion.setEnabled(true);
//        txt_nombres.requestFocus();

        btn_limpiar.setVisibility(View.GONE);

    }

    private void enviarWebService(final NotificacionMuerteBebe notificacion){

        webservice=new DataConnection(this);
        final  String cod_telefono= Helpers.getShared("codigo",this);

        Log.i("notificacion","enviando al web services==>"+notificacion.toValues().toString());


        btn_notificar.setEnabled(false);

        final android.app.AlertDialog dialog=new SpotsDialog(this);
        dialog.show();
        dialog.setMessage("Guardando Notificacion...");


        webservice.EnviarNotificacionMuerteBebe(
                new DataConnection.VolleyCallbackMuerteBebe() {
                    @Override
                    public void onSuccess(JSONObject response_json, Boolean status) {

                        try{
                            if(status ) {

                                JSONObject data = response_json.getJSONObject("data");
                                int id_api=data.getInt("IdNotificacionMuerteBebe");
                                notificacion.setId_api(id_api);
                                notificacion.setSync(1);
                                notificacion.setEstado("enviado");
                                Log.i("response","datos desde el api==>"+data.toString());

                                guardarBaseDatos(notificacion);





                            }
                            else{
                                String msg=response_json.getString("mensaje");

                                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                                Log.i("status","error");

                            }
                        }
                        catch (Exception e){ }

                        btn_notificar.setEnabled(true);
                        dialog.dismiss();

                    }
                    @Override
                    public void ErrorHandler(String error) {
                        Log.i("debug","void ErrorHandler(String error)"+error);
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                        btn_notificar.setEnabled(true);
                        dialog.dismiss();
                    }


                },
                notificacion,cod_telefono
        );


    }

    private void guardarBaseDatos(NotificacionMuerteBebe notificacion){



        try{

            database.insertNotificacionMuerteBebe(notificacion);

            String msg="La notificacion  se envio a su centro de salud correctamente. Gracias!!!";

            if(notificacion.getSync()==0){
                msg="La notificacion de Muerte del Bebe no se pudo enviar al centro de salud. Sin embargo esta se guardo en su celular y se  enviara una vez se conecte a internet. Gracias!!!";
            }


            android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                    .setTitle("NOTIFICACION EXITOSA")
                    .setMessage(msg)
                    .setPositiveButton("VER HISTORIAL DE NOTIFICACIONES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent=new Intent(NotificarMuerteBebeActivity.this,ListaNotificaciones.class);
                            startActivity(intent);
                            finish();

//                                Log.d("MainActivity", "Sending atomic bombs to Jupiter");
                        }
                    })
//                .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                                Log.d("MainActivity", "Aborting mission...");
//                    }
//                })
                    .show();
            dialog.setCanceledOnTouchOutside(false);

        }
        catch (Exception $e){
            Toast.makeText(this,$e.getMessage(),Toast.LENGTH_LONG).show();

        }


    }


    private void notificar() {

        double longitud = 0.0;
        double latitud = 0.0;
//        Location l = gpsTracker.getLocation();

        if (this.location != null) {


            longitud = this.location.getLongitude();
            latitud = this.location.getLatitude();
        }
        else{
            longitud = 0;
            latitud = 0;
        }


            String nombres = txt_nombres.getText().toString();
            String apellidos = txt_apellidos.getText().toString();
            String direccion = txt_direccion.getText().toString();

            String mujer_id = "";
            if (this.mujer != null) {

                if (!rb_nacidomuerto.isChecked() && !rb_nacidovivo.isChecked() && !rb_noconozco.isChecked()) {

                    Helpers.showTooltips(this, radio_group, "SELECCIONA UNA DE LAS  OPCIONES");
                    return;
                }

                boolean validate_muerte=database.validarMujertieneNotificacionMuerte(this.mujer);
                boolean validate=database.validarNotificacionMismoAnio(this.mujer,"notificaciones_muerte_bebe");
                if(validate_muerte){
                    android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                            .setTitle("ERROR")
                            .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE MUERTE REGISTRADA")
                            .show();
                    return;
                }
                if(validate){
                    android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                            .setTitle("ERROR")
                            .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE MUERTE DE BEBE REGISTRADA ESTE AÑO")
                            .show();
                    return;
                }

                mujer_id = this.mujer.getId();


                NotificacionMuerteBebe notificacion = new NotificacionMuerteBebe(
                        nacido,
                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
                        0,
                        "no enviado",
                        0,
                        nombres,
                        "",
                        apellidos,
                        direccion,
                        "",
                        0,
                        latitud,
                        longitud,
                        mujer_id

                );
                notificacion.setEdit(0);

                Boolean connectivity = network.verificarConexion();
                if (connectivity) {
                    enviarWebService(notificacion);

                } else {

                    guardarBaseDatos(notificacion);
                }
            }
            else{
                Helpers.showTooltips(this, btn_buscar, "SELECCIONA A UNA MUJER");

//                mujer_id = "";
//
//
//                NotificacionMuerteBebe notificacion = new NotificacionMuerteBebe(
//                        nacido,
//                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
//                        0,
//                        0,
//                        nombres,
//                        "",
//                        apellidos,
//                        direccion,
//                        "",
//                        0,
//                        latitud,
//                        longitud,
//                        mujer_id
//
//                );
//                notificacion.setEdit(0);
//
//                Boolean connectivity = network.verificarConexion();
//                if (connectivity) {
//                    enviarWebService(notificacion);
//
//                } else {
//
//                    guardarBaseDatos(notificacion);
//                }


            }

    }



}
