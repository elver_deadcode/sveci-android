package evsoft.deadcode.com.sveci;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


import org.json.JSONArray;
import org.json.JSONObject;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.NotificacionEmbarazada;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteBebe;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteMujer;
import evsoft.deadcode.com.sveci.model.NotificacionParto;



public class MainActivity extends AppCompatActivity {

     Database database;
     Toolbar toolbar;
     DataConnection dataConnection;
     Button ButtonSync;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.menuConfiguracion:

                Intent intent_configuraciones = new Intent(this,configuracionActivity.class);
                startActivity(intent_configuraciones);
                break;

            case R.id.menuSalir:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbarIndex);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonSync=(Button) findViewById(R.id.btnSyncronizar);

        CardView cv_mujerEmbarazada = (CardView) findViewById(R.id.card_mujerembarazada);
        CardView cv_muertemujer = (CardView) findViewById(R.id.card_muertemujer);
        CardView cv_muertebebe = (CardView) findViewById(R.id.card_muertebebe);
        CardView cv_parto = (CardView) findViewById(R.id.card_parto);
        CardView cv_registromujer = (CardView) findViewById(R.id.card_registromujer);
        CardView cv_historialnotificaciones = (CardView) findViewById(R.id.card_historialnotificaciones);
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        final String codigo = sharedPreferences.getString("codigo", "");
        database=new Database(this);

        dataConnection=new DataConnection(this);
        ButtonSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recuperarData(codigo);
            }
        });



        cv_mujerEmbarazada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarMujerEmbarazada = new Intent(view.getContext(),NotificarMujerEmbarazadaActivity.class);
                startActivity(intent_notificarMujerEmbarazada);
            }
        });

        cv_muertemujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarmuertemujer = new Intent(view.getContext(),NotificarMuerteMujerActivity.class);
                startActivity(intent_notificarmuertemujer);
            }
        });

        cv_muertebebe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarmuertebebe = new Intent(view.getContext(),NotificarMuerteBebeActivity.class);
                startActivity(intent_notificarmuertebebe);
            }
        });

        cv_parto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarparto = new Intent(view.getContext(),NotificarPartoActivity.class);
                startActivity(intent_notificarparto);
            }
        });

        cv_registromujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_registromujer = new Intent(view.getContext(),ListaMujeresActivity.class);
                intent_registromujer.putExtra("retroceder",false);
                startActivity(intent_registromujer);
            }
        });
        cv_historialnotificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_historial = new Intent(view.getContext(),ListaNotificaciones.class);
                intent_historial.putExtra("retroceder",false);
                startActivity(intent_historial);
            }
        });

        showToolbar("SVEC-SMN  MENU PRINCIPAL ",false);




    }
    protected  void   procesarNotificacionEmbarazo(JSONArray jsonembarazo_array){
        try{
            for (int i = 0; i < jsonembarazo_array.length(); i++) {
                JSONObject o = jsonembarazo_array.getJSONObject(i);
                String id = o.getString("IdAndroid");
                boolean existe_registro= database.buscarRegistroNotificacionPorId(id,"notificaciones_embarazo");
                if(!existe_registro){
                    NotificacionEmbarazada noti =new NotificacionEmbarazada();
                    noti.setId(id);
                    noti.setTipo("embarazo");
                    noti.setFecha(o.getString("fechaRegistro"));
                    noti.setEdit(0);
                    noti.setEstado("enviado");
                    noti.setNombres(o.getString("nombres"));
                    noti.setApellidos(o.getString("apellidos_completo"));
                    noti.setDireccion(o.getString("direccion"));
                    noti.setError("");
                    noti.setLatitud(o.getDouble("latitud"));
                    noti.setLongitud(o.getDouble("longitud"));
                     noti.setEdad(o.getInt("edad"));
                     noti.setMujer_id(o.getString("idMujerAndroid"));
                     noti.setId_api(o.getInt("IdNotificacionEmbarazo"));
                     noti.setSync(1);
                   database.insertNotificacionEmbarazo(noti);

                }
                 }
        }
        catch (Exception e){
            Log.d("recuperar", e.getMessage());
        }
    }


    protected  void   procesarNotificacionMuerteMujer(JSONArray json_array){
        try{
            for (int i = 0; i < json_array.length(); i++) {
                JSONObject o = json_array.getJSONObject(i);
                String id = o.getString("IdAndroid");
                boolean existe_registro= database.buscarRegistroNotificacionPorId(id,"notificaciones_muerte_mujer");
                if(!existe_registro){
                    NotificacionMuerteMujer noti =new NotificacionMuerteMujer();
                    noti.setId(id);
                    noti.setFecha(o.getString("fechaRegistro"));
                    noti.setEdit(0);
                    noti.setEstado("enviado");
                    noti.setNombres(o.getString("nombres"));
                    noti.setApellidos(o.getString("apellidos_completo"));
                    noti.setDireccion(o.getString("direccion"));
                    noti.setError("");
                    noti.setLatitud(o.getDouble("latitud"));
                    noti.setLongitud(o.getDouble("longitud"));
                    noti.setEdad(o.getInt("edad"));
                    noti.setMujer_id(o.getString("idMujerAndroid"));
                    noti.setId_api(o.getInt("IdNotificacionMuerteMujer"));
                    noti.setEmbarazada(o.getInt("estaba_embarazada"));
                    noti.setTelefono(o.getString("telefono"));
                    noti.setSync(1);
                    database.insertNotificacionMuerteMujer(noti);

                }
            }
        }
        catch (Exception e){
            Log.d("recuperar", e.getMessage());
        }
    }

    protected  void   procesarNotificacionMuerteBebe(JSONArray json_array){
        try{
            for (int i = 0; i < json_array.length(); i++) {
                JSONObject o = json_array.getJSONObject(i);
                String id = o.getString("IdAndroid");
                boolean existe_registro= database.buscarRegistroNotificacionPorId(id,"notificaciones_muerte_bebe");
                if(!existe_registro){
                    NotificacionMuerteBebe noti =new NotificacionMuerteBebe();
                    noti.setId(id);
                    noti.setFecha(o.getString("fechaRegistro"));
                    noti.setEdit(0);
                    noti.setEstado("enviado");
                    noti.setNombres(o.getString("nombres"));
                    noti.setApellidos(o.getString("apellidos_completo"));
                    noti.setDireccion(o.getString("direccion"));
                    noti.setError("");
                    noti.setLatitud(o.getDouble("latitud"));
                    noti.setLongitud(o.getDouble("longitud"));
                    noti.setEdad(o.getInt("edad"));
                    noti.setMujer_id(o.getString("idMujerAndroid"));
                    noti.setId_api(o.getInt("IdNotificacionMuerteBebe"));
                    noti.setNacido(o.getInt("estaba_nacido"));
                    noti.setTelefono(o.getString("telefono"));
                    noti.setSync(1);
                    database.insertNotificacionMuerteBebe(noti);

                }
            }
        }
        catch (Exception e){
            Log.d("recuperar", e.getMessage());
        }
    }

    protected  void   procesarNotificacionParto(JSONArray json_array){
        try{
            for (int i = 0; i < json_array.length(); i++) {
                JSONObject o = json_array.getJSONObject(i);
                String id = o.getString("IdAndroid");
                boolean existe_registro= database.buscarRegistroNotificacionPorId(id,"notificaciones_parto");
                if(!existe_registro){
                    NotificacionParto noti =new NotificacionParto();
                    noti.setId(id);
                    noti.setFecha(o.getString("fechaRegistro"));
                    noti.setEdit(0);
                    noti.setEstado("enviado");
                    noti.setNombres(o.getString("nombres"));
                    noti.setApellidos(o.getString("apellidos_completo"));
                    noti.setDireccion(o.getString("direccion"));
                    noti.setError("");
                    noti.setLatitud(o.getDouble("latitud"));
                    noti.setLongitud(o.getDouble("longitud"));
                    noti.setEdad(o.getInt("edad"));
                    noti.setMujer_id(o.getString("idMujerAndroid"));
                    noti.setId_api(o.getInt("IdNotificacionParto"));
                    noti.setAtendido_por(o.getString("atendido"));
                    noti.setTelefono(o.getString("telefono"));
                    noti.setSync(1);
                    database.insertNotificacionParto(noti);

                }
            }
        }
        catch (Exception e){
            Log.d("recuperar", e.getMessage());
        }
    }
    protected void procesarRegistrosMujeres(JSONArray jsonmujeres_array){

        try {
            for (int i = 0; i < jsonmujeres_array.length(); i++) {
                JSONObject o = jsonmujeres_array.getJSONObject(i);


                    String id = o.getString("idAndroid");
                JSONObject persona = o.getJSONObject("persona");


                boolean existe_registro=database.buscarRegistroMujerPorId(id);
                    if(!existe_registro){
                       Mujer mujer = new Mujer();
                        mujer.setId(id);
                        mujer.setEdit(0);
                        mujer.setNombres(persona.getString("nombres"));
                        mujer.setApellidopaterno(persona.getString("primerApellido"));
                        mujer.setApellidomaterno(persona.getString("segundoApellido"));
                        mujer.setFechanacimiento(persona.getString("fecha_nacimiento_android"));
                        mujer.setCarnet(persona.getString("numeroCarnet"));
                        mujer.setDireccion(persona.getString("direccion"));
                        mujer.setTelefono(persona.getString("celular"));
                        mujer.setSync(1);
                        mujer.setAdscrito(persona.getInt("adscrito_id"));
                        mujer.setComplemento(persona.getString("complemento"));
                        database.insertMujer(mujer);
                    }




            }
        }
        catch (Exception e){
                Log.d("recuperar", e.getMessage());
        }
    }

     void recuperarData(String codigo){
         final android.app.AlertDialog dialog=new SpotsDialog(this);
         dialog.show();
         dialog.setMessage("Syncronizaciones Notificaciones...");
         new java.util.Timer().schedule(
                 new java.util.TimerTask() {
                     @Override
                     public void run() {

                         dialog.dismiss();

                     }
                 },
                 5000
         );
        dataConnection.RecuperarData(new DataConnection.recuperarDataCallback() {
            @Override
            public void onSuccess(JSONObject result, Boolean res) {
                if (res == true) {
                    JSONArray jsonmujeres_array = new JSONArray();
                    JSONArray jsonembarazo_array = new JSONArray();
                    JSONArray jsonmuertemujer_array = new JSONArray();
                    JSONArray jsonmuertebebe_array = new JSONArray();
                    JSONArray jsonparto_array = new JSONArray();
                    try {
                        jsonmujeres_array = result.getJSONArray("registros_mujeres");
                        jsonembarazo_array = result.getJSONArray("notificaciones_embarazo");
                        jsonmuertemujer_array = result.getJSONArray("notificaciones_muertemujer");
                        jsonmuertebebe_array = result.getJSONArray("notificaciones_muertebebe");
                        jsonparto_array = result.getJSONArray("notificaciones_parto");
                        if (jsonmujeres_array.length() > 0) {
                            procesarRegistrosMujeres(jsonmujeres_array);
                        }
                        if (jsonembarazo_array.length() > 0) {
                            procesarNotificacionEmbarazo(jsonembarazo_array);
                        }
                        if (jsonmuertemujer_array.length() > 0) {
                            procesarNotificacionMuerteMujer(jsonmuertemujer_array);
                        }
                        if (jsonmuertebebe_array.length() > 0) {
                            procesarNotificacionMuerteBebe(jsonmuertebebe_array);
                        }
                        if (jsonparto_array.length() > 0) {
                            procesarNotificacionParto(jsonparto_array);
                        }
                        Log.d("recuperar",jsonmujeres_array.toString());
                        mostrarDialog("SINCRONIZACION EXITOSA","La sincronizacion de tu informacion con el servidor se ha realizado correctamente");
                    }
                    catch (Exception e){
                        Log.d("recuperar",e.getMessage());

                    }
                }
                else{

                    try {
                        String error = result.getString("error");
                        Log.d("recuperar", error);
                        mostrarDialog("ERROR",error);

                    }
                    catch (Exception e){

                    }

                }

            }

            @Override
            public void ErrorHandler(String error) {
                Log.d("recuperar",error);
            }
        },codigo);
     }


            void mostrarDialog(String titulo,String mensaje){
                android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog)
                        .setTitle(titulo)
                        .setMessage(mensaje)
                        .setPositiveButton("CERRAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        })

                        .show();
            }
}
