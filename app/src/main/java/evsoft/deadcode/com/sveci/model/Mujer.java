package evsoft.deadcode.com.sveci.model;

import android.content.ContentValues;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import evsoft.deadcode.com.sveci.Helpers.Helpers;

public class Mujer  implements Serializable{

    public static final int NUM_COLUMNS = 14;
    private String id;
    private String foto;
    private String nombres;
    private String apellidomaterno;
    private String apellidopaterno;
    private  String fechanacimiento;
    private  String carnet;
    private  String expedido;
    private  String direccion;
    private  String telefono;
    private Integer sync;
    private Integer edit;
    private String error;
    private Integer adscrito;
    private String complemento;

    public Integer getEdit() {
        return edit;
    }

    public void setEdit(Integer edit) {
        this.edit = edit;
    }

    public String getError() {
        return error;
    }
    public String getInicial(){
        String firstLetter = Character.toString(this.nombres.charAt(0));
        return firstLetter;

    }

    public void setError(String error) {
        this.error = error;
    }

    public Mujer(){}

    public Mujer( String foto, String nombres, String apellidomaterno, String apellidopaterno, String fechanacimiento, String carnet, String expedido, String direccion, String telefono, Integer sync,Integer adscrito_id,String complemento) {
        this.id = generateUUID();
        this.foto = foto;
        this.nombres = nombres;
        this.apellidomaterno = apellidomaterno;
        this.apellidopaterno = apellidopaterno;
        this.fechanacimiento = fechanacimiento;
        this.carnet = carnet;
        this.expedido = expedido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.sync = sync;
        this.adscrito=adscrito_id;
        this.complemento=complemento;
    }

    public int Edad(){
        int edad= Helpers.getEdad(this.fechanacimiento);
        return  edad;
    }
    public Date getFechaDate(){
        Date _date=null;
        _date=Helpers.getFecha(this.fechanacimiento,"y-m-d H:m:s");
        return _date;
    }

    public String generateUUID(){
        return UUID.randomUUID().toString();
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getExpedido() {
        return expedido;
    }

    public void setExpedido(String expedido) {
        this.expedido = expedido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getSync() {
        return sync;
    }

    public void setSync(Integer sync) {
        this.sync = sync;
    }

    public void setAdscrito(Integer adscrito) {
        this.adscrito = adscrito;
    }
    public Integer getAdscrito() {
        return adscrito;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
    public String getComplemento() {
        return complemento;
    }

    public ContentValues toValues(){
        ContentValues contentValues=new ContentValues(NUM_COLUMNS);
        contentValues.put("id",id);
        contentValues.put("foto",foto);
        contentValues.put("apellidomaterno",apellidomaterno);
        contentValues.put("nombres",nombres);
        contentValues.put("apellidopaterno",apellidopaterno);
        contentValues.put("fechanacimiento",fechanacimiento);
        contentValues.put("carnet",carnet);
        contentValues.put("expedido",expedido);
        contentValues.put("direccion",direccion);
        contentValues.put("telefono",telefono);
        contentValues.put("sync",sync);
        contentValues.put("edit",edit);
        contentValues.put("error",error);
        contentValues.put("adscrito",adscrito);
        contentValues.put("complemento",complemento);

     return  contentValues;
    }
}
