package evsoft.deadcode.com.sveci.Clases;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import evsoft.deadcode.com.sveci.Helpers.Helpers;

public class ValidateHelper {

    private Context context;

    public ValidateHelper(Context context) {
        this.context = context;
    }

    public Boolean  isEditTextFilled(EditText editText, TextInputLayout textInputLayout, String message){
        String value=editText.getText().toString().trim();
        if(value.isEmpty()){
            textInputLayout.setError(message);
            hideKeyboardFrom(editText);
            return false;
        }
        else
        {
            textInputLayout.setErrorEnabled(false);
        }
        return true;
    }

    public Boolean isFechaNacimientoValida (EditText editText, TextInputLayout textInputLayout, String message,int anios_min,int anios_max ){

        String value=editText.getText().toString().trim();
        int edad= Helpers.getEdad(value);

        if(edad<anios_min || edad >anios_max) {
            textInputLayout.setError(message);
            hideKeyboardFrom(editText);
            return false;
        }


        else
        {
            textInputLayout.setErrorEnabled(false);
        }
        return true;
    }
    public Boolean isEditTextEmail(EditText editText, TextInputLayout textInputLayout, String message) {
        String value = editText.getText().toString().trim();
        if (value.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches()) {
            textInputLayout.setError(message);
            hideKeyboardFrom(editText);
            return false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
        return true;
    }

    public Boolean isSpinner(Spinner spinner, final String valor_incorrecto , TextInputLayout textInputLayout, String message) {


        final boolean[] isvalidate = {true};
        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItemText = (String) adapterView.getItemAtPosition(i);
                if (selectedItemText.equals(valor_incorrecto)) {
                    isvalidate[0] = false;

                } else {
                    isvalidate[0] = true;
                }
            }


            });

        if (!isvalidate[0]) {
            textInputLayout.setError(message);
            return false;
        } else {
//            textInputLayout.setErrorEnabled(false);
        }
        return true;
    }


    private void hideKeyboardFrom(View view) {
        InputMethodManager inputMethodManager=(InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
