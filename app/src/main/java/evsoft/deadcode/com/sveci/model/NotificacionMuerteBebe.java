package evsoft.deadcode.com.sveci.model;

import android.content.ContentValues;

import java.util.UUID;

public class NotificacionMuerteBebe {


          private static final int NUM_ROWS =16;

          private String id;
          private int nacido;
          private String fecha;
          private int sync;
          private int edit;
          private String nombres;
          private String estado;
          private String error;
          private String apellidos;
          private String direccion;
          private String telefono;
          private int edad;
          private Double latitud;
          private Double longitud;
          private String mujer_id;
          private int id_api;

    public NotificacionMuerteBebe(){}

    public NotificacionMuerteBebe(int nacido, String fecha, int sync, String estado,int edit, String nombres,  String error, String apellidos, String direccion, String telefono, int edad, Double latitud, Double longitud, String mujer_id) {
        this.id = generateUUID();

        this.nacido = nacido;
        this.fecha = fecha;
        this.sync = sync;
        this.edit = edit;
        this.nombres = nombres;
        this.estado = estado;
        this.error = error;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.edad = edad;
        this.latitud = latitud;
        this.longitud = longitud;
        this.mujer_id = mujer_id;
        this.id_api = id_api;
    }

    private String generateUUID(){
        return UUID.randomUUID().toString();
    }


    public static int getNumRows() {
        return NUM_ROWS;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNacido() {
        return nacido;
    }

    public void setNacido(int nacido) {
        this.nacido = nacido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public int getEdit() {
        return edit;
    }

    public void setEdit(int edit) {
        this.edit = edit;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getMujer_id() {
        return mujer_id;
    }

    public void setMujer_id(String mujer_id) {
        this.mujer_id = mujer_id;
    }

    public int getId_api() {
        return id_api;
    }

    public void setId_api(int id_api) {
        this.id_api = id_api;
    }

    public ContentValues toValues(){
        ContentValues contentValues=new ContentValues(NUM_ROWS);

       contentValues.put("id",id);
       contentValues.put("nacido",nacido);
       contentValues.put("fecha",fecha);
       contentValues.put("sync",sync);
       contentValues.put("edit",edit);
       contentValues.put("nombres",nombres);
       contentValues.put("estado",estado);
       contentValues.put("error",error);
       contentValues.put("apellidos",apellidos);
       contentValues.put("direccion",direccion);
       contentValues.put("telefono",telefono);
       contentValues.put("edad",edad);
       contentValues.put("latitud",latitud);
       contentValues.put("longitud",longitud);
       contentValues.put("mujer_id",mujer_id);
       contentValues.put("id_api",id_api);


        return contentValues;
    }
}
