package evsoft.deadcode.com.sveci.Clases;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.SQLConstants;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.Notificacion;
import evsoft.deadcode.com.sveci.model.NotificacionEmbarazada;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteBebe;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteMujer;
import evsoft.deadcode.com.sveci.model.NotificacionParto;

public class Database {

  private Context context;
  private SQLiteDatabase sqLiteDatabase;
  private SQLiteOpenHelper sqLiteOpenHelper;

    public Database(Context context) {
        this.context = context;
        this.sqLiteOpenHelper=new DBConnect(context);
        this.sqLiteDatabase=sqLiteOpenHelper.getWritableDatabase();
    }

     public void open(){
        this.sqLiteDatabase=sqLiteOpenHelper.getWritableDatabase();
     }
     public void close(){
        sqLiteOpenHelper.close();
     }



       //CONSULTAS TABLA MUJER
    public void insertMujer(Mujer mujer){
        ContentValues contentValues=mujer.toValues();
        sqLiteDatabase.insert(SQLConstants.DB_TABLE_MUJER,null,contentValues);
    }

    public void updateMujer(Mujer mujer)
    {
        ContentValues contentValues=mujer.toValues();
        String id=contentValues.getAsString("id");
        sqLiteDatabase.update(SQLConstants.DB_TABLE_MUJER, contentValues, "id=?", new String[]{id});

    }



    public long getNumRowMujeres(){
        return DatabaseUtils.queryNumEntries(sqLiteDatabase,SQLConstants.DB_TABLE_MUJER);
    }

    //CONSULTAS TABLA NOTIFICACIONES

    public void insertNotificacionEmbarazo(NotificacionEmbarazada notificacion){
        ContentValues contentValues=notificacion.toValues();
        sqLiteDatabase.insert(SQLConstants.DB_TABLE_NOTIFICACIONES_EMBARZAZO,null,contentValues);
    }

    public void updateSyncApiNotificacionEmbarazo(String id,int sync,int id_api){

        String strSQL = "UPDATE "+SQLConstants.DB_TABLE_NOTIFICACIONES_EMBARZAZO + " SET sync ="+sync+" , id_api="+id_api+" WHERE id = "+ id;

        sqLiteDatabase.execSQL(strSQL);
    }

    public long getNumRowNotificaciones(){
        return DatabaseUtils.queryNumEntries(sqLiteDatabase,SQLConstants.DB_TABLE_NOTIFICACIONES_EMBARZAZO);
    }




    //*********************NOTIFICACIONES MUERTE MUJER*********************
    public void insertNotificacionMuerteMujer(NotificacionMuerteMujer notificacion){
        ContentValues contentValues=notificacion.toValues();
        sqLiteDatabase.insert(SQLConstants.DB_TABLE_NOTIFICACIONES_MUERTE_MUJER,null,contentValues);
    }





    //************************ FIN NOTIFICACIONES MUERTE MUJER*****************

    //*********************NOTIFICACIONES MUERTE BEBE*********************
    public void insertNotificacionMuerteBebe(NotificacionMuerteBebe notificacion){
        ContentValues contentValues=notificacion.toValues();
        sqLiteDatabase.insert(SQLConstants.DB_TABLE_NOTIFICACIONES_MUERTE_BEBE,null,contentValues);
    }



    //************************ FIN NOTIFICACIONES MUERTE BEBE*****************


    //*********************NOTIFICACIONES PARTO*********************
    public void insertNotificacionParto(NotificacionParto notificacion){
        ContentValues contentValues=notificacion.toValues();
        sqLiteDatabase.insert(SQLConstants.DB_TABLE_NOTIFICACIONES_PARTO,null,contentValues);
    }



    //************************ FIN NOTIFICACIONES PARTO*****************



    //************************ SACANDO LA INFO PARA EL POJO DE NOTIFICACION *************************

    public ArrayList<Notificacion> obtenerNotificacionPorTabla(String tabla){
                ArrayList<Notificacion> lista_notificaciones = new ArrayList<>();
            try {
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT "+tabla+".id, "+tabla+".sync, "+tabla+".edit,"+tabla+".error, "+tabla+".fecha, "+tabla+".estado,"+tabla+".nombres,"+tabla+".apellidos,mujeres.direccion,mujeres.telefono,mujeres.carnet FROM " + tabla + " INNER JOIN mujeres ON mujeres.id = " + tabla + ".mujer_id", null);


                while (cursor.moveToNext()) {
                    Notificacion notificacion = new Notificacion();
                    String noti="";
                    switch (tabla){
                        case "notificaciones_embarazo":noti="NME"; break;
                        case "notificaciones_muerte_mujer":noti="NMM"; break;
                        case "notificaciones_muerte_bebe":noti="NMB"; break;
                        case "notificaciones_parto":noti="NP"; break;
                    }


                    notificacion.setId(cursor.getString(cursor.getColumnIndex("id")));
                    notificacion.setSync(cursor.getInt(cursor.getColumnIndex("sync")));
                    notificacion.setFecha(cursor.getString(cursor.getColumnIndex("fecha")));
                    notificacion.setEstado(cursor.getString(cursor.getColumnIndex("estado")));
                    notificacion.setNombres(cursor.getString(cursor.getColumnIndex("nombres")));
                    notificacion.setApellidos(cursor.getString(cursor.getColumnIndex("apellidos")));
                    notificacion.setDireccion(cursor.getString(cursor.getColumnIndex("direccion")));
                    notificacion.setTelefono(cursor.getString(cursor.getColumnIndex("telefono")));
                    notificacion.setCarnet(cursor.getString(cursor.getColumnIndex("carnet")));
                    notificacion.setEdit(cursor.getInt(cursor.getColumnIndex("edit")));
                    notificacion.setError(cursor.getString(cursor.getColumnIndex("error")));
                    notificacion.setNotificacion(noti);
                    lista_notificaciones.add(notificacion);
                }

            }
            catch(Exception e){
                Log.e("ERROR SQL",e.getMessage());
            }
        return lista_notificaciones;

    }

    public ArrayList<Notificacion> getNotificaciones(){
        ArrayList<Notificacion> lista_notificaciones=new ArrayList<>();
        ArrayList<Notificacion> embarazo=this.obtenerNotificacionPorTabla("notificaciones_embarazo");
        ArrayList<Notificacion> muerte_mujer=this.obtenerNotificacionPorTabla("notificaciones_muerte_mujer");
        ArrayList<Notificacion> muerte_bebe=this.obtenerNotificacionPorTabla("notificaciones_muerte_bebe");
        ArrayList<Notificacion> parto=this.obtenerNotificacionPorTabla("notificaciones_parto");

        lista_notificaciones.addAll(embarazo);
        lista_notificaciones.addAll(muerte_mujer);
        lista_notificaciones.addAll(muerte_bebe);
        lista_notificaciones.addAll(parto);
        Collections.sort(lista_notificaciones, new Comparator<Notificacion>() {
            @Override
            public int compare(Notificacion noti_a, Notificacion noti_b) {
                Date a = noti_a.getFechaDate();
                Date b = noti_b.getFechaDate();

                if (a.getTime()>b.getTime())
                    return -1;

                else
                    return 1;
            }


        });
        return lista_notificaciones;

    }

    //************************ SACANDO LA INFO PARA EL POJO DE NOTIFICACION *************************


    public ArrayList<Mujer> getMujeresAll(){
        ArrayList<Mujer> listaMujeres=new ArrayList<>();

          Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM mujeres",null);

        while(cursor.moveToNext()){
               Mujer mujer =new Mujer();
               mujer.setId(cursor.getString(cursor.getColumnIndex("id")));
               mujer.setFoto(cursor.getString(cursor.getColumnIndex("foto")));
               mujer.setNombres(cursor.getString(cursor.getColumnIndex("nombres")));
               mujer.setApellidomaterno(cursor.getString(cursor.getColumnIndex("apellidoMaterno")));
               mujer.setApellidopaterno(cursor.getString(cursor.getColumnIndex("apellidoPaterno")));
               mujer.setFechanacimiento(cursor.getString(cursor.getColumnIndex("fechanacimiento")));
               mujer.setCarnet(cursor.getString(cursor.getColumnIndex("carnet")));
               mujer.setExpedido(cursor.getString(cursor.getColumnIndex("expedido")));
               mujer.setDireccion(cursor.getString(cursor.getColumnIndex("direccion")));
               mujer.setTelefono(cursor.getString(cursor.getColumnIndex("telefono")));
               mujer.setSync(cursor.getInt(cursor.getColumnIndex("sync")));
               mujer.setAdscrito(cursor.getInt(cursor.getColumnIndex("adscrito")));
               mujer.setComplemento(cursor.getString(cursor.getColumnIndex("complemento")));

                listaMujeres.add(mujer);
      }

        return listaMujeres;
    }

    public void borrar(){
        this.sqLiteOpenHelper.onUpgrade(sqLiteDatabase,1,2);
    }



    public JsonArray MujeresSync(){
        JsonArray listaMujeres= new JsonArray();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM mujeres where sync=0 OR edit=1",null);

        while(cursor.moveToNext()){
            JsonObject jsonObject=new JsonObject();
//            Mujer mujer =new Mujer();
            jsonObject.addProperty("id",cursor.getString(cursor.getColumnIndex("id")));
            jsonObject.addProperty("foto",cursor.getString(cursor.getColumnIndex("foto")));
            jsonObject.addProperty("nombres",cursor.getString(cursor.getColumnIndex("nombres")));
            jsonObject.addProperty("apellidoMaterno",cursor.getString(cursor.getColumnIndex("apellidoMaterno")));
            jsonObject.addProperty("apellidoPaterno",cursor.getString(cursor.getColumnIndex("apellidoPaterno")));
            jsonObject.addProperty("fechanacimiento",cursor.getString(cursor.getColumnIndex("fechanacimiento")));
            jsonObject.addProperty("carnet",cursor.getString(cursor.getColumnIndex("carnet")));
            jsonObject.addProperty("expedido",cursor.getString(cursor.getColumnIndex("expedido")));
            jsonObject.addProperty("direccion",cursor.getString(cursor.getColumnIndex("direccion")));
            jsonObject.addProperty("telefono",cursor.getString(cursor.getColumnIndex("telefono")));
            jsonObject.addProperty("sync",cursor.getInt(cursor.getColumnIndex("sync")));
            jsonObject.addProperty("edit",cursor.getInt(cursor.getColumnIndex("edit")));
            jsonObject.addProperty("adscrito",cursor.getInt(cursor.getColumnIndex("adscrito")));
            jsonObject.addProperty("complemento",cursor.getInt(cursor.getColumnIndex("complemento")));


            listaMujeres.add(jsonObject);
        }
        return listaMujeres;
    }



    public JsonArray NotificacionEmbarazoSync(){
        JsonArray listaNotificacion= new JsonArray();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notificaciones_embarazo where sync=0 OR sync=1 ",null);

        while(cursor.moveToNext()){

            JsonObject  jsonObject=new JsonObject();


//           NotificacionEmbarazada noti=new NotificacionEmbarazada();
             jsonObject.addProperty("id",cursor.getString(cursor.getColumnIndex("id")));
             jsonObject.addProperty("tipo",cursor.getString(cursor.getColumnIndex("tipo")));
             jsonObject.addProperty("fecha",cursor.getString(cursor.getColumnIndex("fecha")));
             jsonObject.addProperty("sync",cursor.getInt(cursor.getColumnIndex("sync")));
             jsonObject.addProperty("estado",cursor.getString(cursor.getColumnIndex("estado")));
             jsonObject.addProperty("nombres",cursor.getString(cursor.getColumnIndex("nombres")));
             jsonObject.addProperty("apellidos",cursor.getString(cursor.getColumnIndex("apellidos")));
             jsonObject.addProperty("direccion",cursor.getString(cursor.getColumnIndex("direccion")));
             jsonObject.addProperty("telefono",cursor.getString(cursor.getColumnIndex("telefono")));
             jsonObject.addProperty("edad",cursor.getInt(cursor.getColumnIndex("edad")));
             jsonObject.addProperty("latitud",cursor.getDouble(cursor.getColumnIndex("latitud")));
             jsonObject.addProperty("longitud",cursor.getDouble(cursor.getColumnIndex("longitud")));
             jsonObject.addProperty("mujer_id",cursor.getString(cursor.getColumnIndex("mujer_id")));
             jsonObject.addProperty("id_api",cursor.getInt(cursor.getColumnIndex("id_api")));
             jsonObject.addProperty("edit",cursor.getInt(cursor.getColumnIndex("edit")));
             jsonObject.addProperty("error",cursor.getString(cursor.getColumnIndex("error")));



            listaNotificacion.add(jsonObject);
        }
        return listaNotificacion;
    }

    public JsonArray NotificacionMuerteMujerSync(){
        JsonArray listaNotificacion= new JsonArray();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notificaciones_muerte_mujer  where sync=0 OR sync=1",null);

        while(cursor.moveToNext()){

            JsonObject  jsonObject=new JsonObject();

            jsonObject.addProperty("id",cursor.getString(cursor.getColumnIndex("id")));
            jsonObject.addProperty("embarazada",cursor.getString(cursor.getColumnIndex("embarazada")));
            jsonObject.addProperty("fecha",cursor.getString(cursor.getColumnIndex("fecha")));
            jsonObject.addProperty("sync",cursor.getInt(cursor.getColumnIndex("sync")));
            jsonObject.addProperty("estado",cursor.getString(cursor.getColumnIndex("estado")));
            jsonObject.addProperty("nombres",cursor.getString(cursor.getColumnIndex("nombres")));
            jsonObject.addProperty("apellidos",cursor.getString(cursor.getColumnIndex("apellidos")));
            jsonObject.addProperty("direccion",cursor.getString(cursor.getColumnIndex("direccion")));
            jsonObject.addProperty("telefono",cursor.getString(cursor.getColumnIndex("telefono")));
            jsonObject.addProperty("edad",cursor.getInt(cursor.getColumnIndex("edad")));
            jsonObject.addProperty("latitud",cursor.getDouble(cursor.getColumnIndex("latitud")));
            jsonObject.addProperty("longitud",cursor.getDouble(cursor.getColumnIndex("longitud")));
            jsonObject.addProperty("mujer_id",cursor.getString(cursor.getColumnIndex("mujer_id")));
            jsonObject.addProperty("id_api",cursor.getInt(cursor.getColumnIndex("id_api")));
            jsonObject.addProperty("edit",cursor.getInt(cursor.getColumnIndex("edit")));
            jsonObject.addProperty("error",cursor.getString(cursor.getColumnIndex("error")));



            listaNotificacion.add(jsonObject);
        }
        return listaNotificacion;
    }


    public JsonArray NotificacionMuerteBebeSync(){
        JsonArray listaNotificacion= new JsonArray();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notificaciones_muerte_bebe where sync=0 OR sync=1 ",null);

        while(cursor.moveToNext()){

            JsonObject  jsonObject=new JsonObject();


            jsonObject.addProperty("id",cursor.getString(cursor.getColumnIndex("id")));
            jsonObject.addProperty("nacido",cursor.getString(cursor.getColumnIndex("nacido")));
            jsonObject.addProperty("fecha",cursor.getString(cursor.getColumnIndex("fecha")));
            jsonObject.addProperty("sync",cursor.getInt(cursor.getColumnIndex("sync")));
            jsonObject.addProperty("estado",cursor.getString(cursor.getColumnIndex("estado")));
            jsonObject.addProperty("nombres",cursor.getString(cursor.getColumnIndex("nombres")));
            jsonObject.addProperty("apellidos",cursor.getString(cursor.getColumnIndex("apellidos")));
            jsonObject.addProperty("direccion",cursor.getString(cursor.getColumnIndex("direccion")));
            jsonObject.addProperty("telefono",cursor.getString(cursor.getColumnIndex("telefono")));
            jsonObject.addProperty("edad",cursor.getInt(cursor.getColumnIndex("edad")));
            jsonObject.addProperty("latitud",cursor.getDouble(cursor.getColumnIndex("latitud")));
            jsonObject.addProperty("longitud",cursor.getDouble(cursor.getColumnIndex("longitud")));
            jsonObject.addProperty("mujer_id",cursor.getString(cursor.getColumnIndex("mujer_id")));
            jsonObject.addProperty("id_api",cursor.getInt(cursor.getColumnIndex("id_api")));
            jsonObject.addProperty("edit",cursor.getInt(cursor.getColumnIndex("edit")));
            jsonObject.addProperty("error",cursor.getString(cursor.getColumnIndex("error")));



            listaNotificacion.add(jsonObject);
        }
        return listaNotificacion;
    }

    public JsonArray NotificacionPartoSync(){
        JsonArray listaNotificacion= new JsonArray();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notificaciones_parto where sync=0 OR sync=1 ",null);

        while(cursor.moveToNext()){

            JsonObject  jsonObject=new JsonObject();

            jsonObject.addProperty("id",cursor.getString(cursor.getColumnIndex("id")));
            jsonObject.addProperty("partera",cursor.getString(cursor.getColumnIndex("partera")));
            jsonObject.addProperty("atendido_por",cursor.getString(cursor.getColumnIndex("atendido_por")));
            jsonObject.addProperty("fecha",cursor.getString(cursor.getColumnIndex("fecha")));
            jsonObject.addProperty("sync",cursor.getInt(cursor.getColumnIndex("sync")));
            jsonObject.addProperty("edit",cursor.getInt(cursor.getColumnIndex("edit")));
            jsonObject.addProperty("nombres",cursor.getString(cursor.getColumnIndex("nombres")));
            jsonObject.addProperty("estado",cursor.getString(cursor.getColumnIndex("estado")));
            jsonObject.addProperty("error",cursor.getString(cursor.getColumnIndex("error")));
            jsonObject.addProperty("apellidos",cursor.getString(cursor.getColumnIndex("apellidos")));
            jsonObject.addProperty("direccion",cursor.getString(cursor.getColumnIndex("direccion")));
            jsonObject.addProperty("telefono",cursor.getString(cursor.getColumnIndex("telefono")));
            jsonObject.addProperty("edad",cursor.getInt(cursor.getColumnIndex("edad")));
            jsonObject.addProperty("latitud",cursor.getDouble(cursor.getColumnIndex("latitud")));
            jsonObject.addProperty("longitud",cursor.getDouble(cursor.getColumnIndex("longitud")));
            jsonObject.addProperty("mujer_id",cursor.getString(cursor.getColumnIndex("mujer_id")));
            jsonObject.addProperty("id_api",cursor.getInt(cursor.getColumnIndex("id_api")));

            listaNotificacion.add(jsonObject);
        }
        return listaNotificacion;
    }



    //***********ACTUALIZAR TABLAS DESPUES DE LA SINCRONIZACION**************

    public void updateSyncApiMujer(String id,int sync){

        String strSQL = "UPDATE mujeres SET sync ="+sync+", edit=0  WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }

    public void updateSyncApiEmbarazo(String id,int sync,int id_api,String estado){

        String strSQL = "UPDATE notificaciones_embarazo SET sync ="+sync+" , id_api="+id_api+", estado='"+estado+"' , error='' WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }
    public void updateErrorApiNotificacion(String tabla,String id,String error,String estado){

        String strSQL = "UPDATE "+tabla+" SET error ='"+error+"', estado='"+estado+"' WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }

    public void updateSyncApiMuerteMujer(String id,int sync,int id_api,String estado){

        String strSQL = "UPDATE notificaciones_muerte_mujer SET sync ="+sync+" , id_api="+id_api+" , estado='"+estado+"' WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }

    public void updateSyncApiMuerteBebe(String id,int sync,int id_api,String estado){

        String strSQL = "UPDATE notificaciones_muerte_bebe SET sync ="+sync+" , id_api="+id_api+" , estado='"+estado+"' WHERE id = '"+ id+"'";
           Log.i("sql",strSQL);
        sqLiteDatabase.execSQL(strSQL);
    }

    public void updateSyncApiParto(String id,int sync,int id_api,String estado){

        String strSQL = "UPDATE notificaciones_parto SET sync ="+sync+" , id_api="+id_api+", estado='"+estado+"' WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }

    public void updateErrorMujer(String id,String error){

        String strSQL = "UPDATE mujeres SET   error='"+error+"' WHERE id='"+id+"'";

        try{
            sqLiteDatabase.execSQL(strSQL);
            Log.i("response", "SE ACTUALIZO ERROR DE MUJER:");
        }
        catch (Exception e){
            Log.i("response", "ERROR ACTUALIZAR: " +strSQL+" ERROR="+e);

        }
    }


    //*********** FIN DE ACTUALIZAR TABLAS DESPUES DE LA SINCRONIZACION**************




    //********************* aVALIDAR NOTIFICACIONES ******************************
    public boolean validarNotificacionMismoAnio(Mujer mujer,String tabla){
        boolean result=false;
        String mujer_id=mujer.getId();


            try {
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + tabla + " where mujer_id='" + mujer_id + "'", null);

                if (cursor.moveToFirst()) {
                    String fecha_noti = cursor.getString(cursor.getColumnIndex("fecha"));
                    Date fecha_ini = Helpers.getFecha(fecha_noti,"yyyy-MM-dd");

                    if (Helpers.compararMismoAnio(fecha_ini, Helpers.fechaActual())) {
                        result = true;
                    }
                }
            }
            catch (Exception e){
                Log.e("ERROR:::",e.getMessage());
                result=false;
            }

                    return result;

    }

    public boolean validarCantidadDias(Mujer mujer,String tabla,int dias){
        boolean result=false;
        String mujer_id=mujer.getId();
        int cant_dias=0;


        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + tabla + " where mujer_id='" + mujer_id + "'", null);

            if (cursor.moveToFirst()) {
                String fecha_noti = cursor.getString(cursor.getColumnIndex("fecha"));
                Date fecha_ini = Helpers.getFecha(fecha_noti,"yyyy-MM-dd");
                cant_dias=Math.abs(Helpers.numeroDiasEntreDosFechas( Helpers.fechaActual(),fecha_ini));
                Log.i("dias",String.valueOf(cant_dias));
                if (cant_dias>=dias) {
                    result = true;
                }

            }
            else{
                result = true;
            }
        }
        catch (Exception e){
            Log.e("ERROR:::",e.getMessage());
            result=true;
        }

        return result;

    }

    public boolean buscarRegistroMujerPorId(String id){
        boolean result=false;

        String strSQL = "select id FROM mujeres  WHERE id = '"+ id+"'";
        Cursor cursor = sqLiteDatabase.rawQuery(strSQL, null);
        if (cursor.moveToFirst()) {
            result=true;
        }

        return result;
    }
    public boolean buscarRegistroNotificacionPorId(String id,String noti){
        boolean result=false;

        String strSQL = "select id FROM "+noti+"  WHERE id = '"+ id+"'";
        Cursor cursor = sqLiteDatabase.rawQuery(strSQL, null);
        if (cursor.moveToFirst()) {
            result=true;
        }

        return result;
    }
    public boolean validarMujertieneNotificacionMuerte(Mujer mujer){
        String mujer_id=mujer.getId();
        boolean result=false;
        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notificaciones_muerte_mujer where mujer_id='" + mujer_id + "'", null);
            if (cursor.moveToFirst()) {

                String noti = cursor.getString(cursor.getColumnIndex("id"));
                result = true;
            }
        }
        catch (Exception e){
            Log.e("ERRROR:::", e.getMessage());

        }
        return result;
    }

    public void borrarNotificacion(String notificacion,String id){

        String strSQL = "DELETE FROM "+notificacion+"  WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }
    public void borrarMujer(String id){

        String strSQL = "DELETE FROM mujeres  WHERE id = '"+ id+"'";

        sqLiteDatabase.execSQL(strSQL);
    }

    //*****************************CONSULTAS PARA  PRUEBAS*****************
    public  void setSyncNotificaciones(int sync){
        String strSQL = "UPDATE notificaciones_embarazo SET error='', sync ="+sync;

        sqLiteDatabase.execSQL(strSQL);
         strSQL = "UPDATE notificaciones_muerte_mujer SET error='',sync ="+sync;

        sqLiteDatabase.execSQL(strSQL);
         strSQL = "UPDATE notificaciones_muerte_bebe  SET error='', sync ="+sync;

        sqLiteDatabase.execSQL(strSQL);
        strSQL = "UPDATE notificaciones_parto SET  error='',sync ="+sync;

        sqLiteDatabase.execSQL(strSQL);
    }

    public  void setResetRegistrosMujeres(int sync){
        String strSQL = "UPDATE mujeres SET error='', sync ="+sync;

        sqLiteDatabase.execSQL(strSQL);

    }

}
