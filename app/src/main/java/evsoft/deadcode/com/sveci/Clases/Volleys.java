package evsoft.deadcode.com.sveci.Clases;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Volleys {

    private static  Volleys mVolley=null;
     private RequestQueue mRequestQueue;

    public Volleys(Context context) {
        mRequestQueue= Volley.newRequestQueue(context);
    }

    public static  Volleys  getInstance(Context context){
        if(mVolley==null){
            mVolley=new Volleys(context);
        }
        return  mVolley;
    }


    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }
}
