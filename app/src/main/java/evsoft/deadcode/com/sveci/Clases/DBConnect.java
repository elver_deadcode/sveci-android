package evsoft.deadcode.com.sveci.Clases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import evsoft.deadcode.com.sveci.SQLConstants;

public class DBConnect extends SQLiteOpenHelper {


    public static final int VERSION = 1;

    public DBConnect(Context context) {
        super(context, SQLConstants.DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLConstants.DB_CREATE_TABLE_NOTIFICACIONES_EMBARAZO);
        db.execSQL(SQLConstants.DB_CREATE_TABLE_NOTIFICACIONES_MUERTE_MUJER);
        db.execSQL(SQLConstants.DB_CREATE_TABLE_NOTIFICACIONES_MUERTE_BEBE);
        db.execSQL(SQLConstants.DB_CREATE_TABLE_NOTIFICACIONES_PARTO);
        db.execSQL(SQLConstants.DB_CREATE_TABLE_MUJER);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAntigua, int versionNueva) {

        db.execSQL(SQLConstants.DB_DROP_TABLE_NOTIFICACIONES);
        db.execSQL(SQLConstants.DB_DROP_TABLE_NOTIFICACIONES_MUERTE_MUJER);
        db.execSQL(SQLConstants.DB_DROP_TABLE_NOTIFICACIONES_MUERTE_BEBE);
        db.execSQL(SQLConstants.DB_DROP_TABLE_NOTIFICACIONES_PARTO);
        db.execSQL(SQLConstants.DB_DROP_TABLE_MUJER);

        onCreate(db);

    }




}
