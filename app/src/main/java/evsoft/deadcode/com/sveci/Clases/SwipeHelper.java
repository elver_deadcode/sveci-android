package evsoft.deadcode.com.sveci.Clases;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import evsoft.deadcode.com.sveci.adapter.ListaNotificacionesAdapterRecyclerView;


public class SwipeHelper extends ItemTouchHelper.SimpleCallback {

    ListaNotificacionesAdapterRecyclerView adapter;
    private  RecyclerItemTouchHelperListener listener;



    public SwipeHelper( int dragDirs, int swipeDirs,RecyclerItemTouchHelperListener listener) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN,ItemTouchHelper.LEFT);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if(viewHolder!=null){
            View foregroundView=((ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder)viewHolder).layoutABorrar;
            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View foregroundView=((ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder)viewHolder).layoutABorrar;
         getDefaultUIUtil().onDrawOver(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        View foregroundView=((ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder)viewHolder).layoutABorrar;

        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View foregroundView=((ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder)viewHolder).layoutABorrar;

        getDefaultUIUtil().onDraw(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            listener.onSwiped(viewHolder,direction,viewHolder.getAdapterPosition());
    }
    public interface  RecyclerItemTouchHelperListener{
        void onSwiped(RecyclerView.ViewHolder viewHolder,int direction, int position);
    }
}