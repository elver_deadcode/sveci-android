package evsoft.deadcode.com.sveci.Clases;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import evsoft.deadcode.com.sveci.R;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.NotificacionEmbarazada;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteBebe;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteMujer;
import evsoft.deadcode.com.sveci.model.NotificacionParto;


public class DataConnection extends BaseVolley {

    private Context context;

    private String path;
    private String path_sus;
    private List<NotificacionEmbarazada> listaNotificaciones;
    private NotificacionEmbarazada notificacion;
    private VolleyError volleyError;
    private boolean requestStatus;
    private JSONObject json_data;

    public boolean isRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(boolean requestStatus) {
        this.requestStatus = requestStatus;
    }

    public DataConnection(Context context) {
        super(context);
        this.context=context;
//        this.path=context.getString(R.string.ApiPath);
        this.requestStatus=false;
        this.path=this.getUrl();
        this.path_sus=this.getUrlApiSus();
    }
    private String getUrl(){

        String url="";
        SharedPreferences sharedPreferences =  context.getSharedPreferences("preferences", Context.MODE_PRIVATE);

        final String UrlApi=sharedPreferences.getString("url","");
        if(UrlApi.isEmpty()){

            return context.getString(R.string.ApiPath);
        }
        else

        return UrlApi;

    }

    private String getUrlApiSus(){

        String url="";
        SharedPreferences sharedPreferences =  context.getSharedPreferences("preferences", Context.MODE_PRIVATE);

        final String UrlApi=sharedPreferences.getString("url_sus","");
        if(UrlApi.isEmpty()){

            return "http://suis-ws.minsalud.gob.bo/api";
        }
        else

            return UrlApi;

    }

    public String handlerError(VolleyError error){

        String msg ="Existio un error desconocido";

        if(error instanceof TimeoutError ){

            msg="El servidor tardo en responder ";

        } else if(error instanceof NoConnectionError){
            msg="Existe un error en la conexion";
        }
        else if(error instanceof ServerError){
            msg="Existe un problema en el servidor : "+error.getMessage();
        }


            return  msg+error.getCause();
    }





    //REQUEST API

    public void EnviarCodigodeActivacion(final VolleyCallbackActivar callbackActivar, final String codigo){
        String url = this.path+"/activar";


        StringRequest request=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("res", "onResponse: "+response);
                        try{

                            json_data=new JSONObject(response);
                            boolean status=(Boolean) json_data.get("status");
                            callbackActivar.onSuccessNotificacion(json_data,status);
                            }
                        catch (Exception $e){

                            callbackActivar.onSuccessNotificacion(null,false);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("error-activacion", "onErrorResponse: "+error.getMessage());
                        callbackActivar.ErrorHandler(handlerError(error));

                    }
                }
        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();

                params.put("codigo",codigo);

                 return params;

            }
        };

        addToQueue(request);
    }



    public  void EnviarNotificacion(final VolleyCallback callback, NotificacionEmbarazada notificacion, final String cod_vigilante){

        final ContentValues valores=notificacion.toValues();


        String url = this.path+"/notificar-embarazo";
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{

                 json_data=new JSONObject(response);
                 boolean status=(Boolean) json_data.get("status");



                     callback.onSuccessNotificacion(json_data,status);


                }
                catch (Exception $e){

                      callback.onSuccessNotificacion(null,false);
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();


                params.put("tipo",valores.get("tipo").toString());
                params.put("fecha", valores.get("fecha").toString());
                params.put("nombres", valores.get("nombres")==null?"":valores.get("nombres").toString());
                params.put("apellidos", valores.get("apellidos")==null?"":valores.get("apellidos").toString());
                params.put("direccion", valores.get("direccion")==null?"":valores.get("direccion").toString());
                params.put("telefono", valores.get("telefono")==null?"":valores.get("telefono").toString());
                params.put("edad", valores.get("edad")==null?"0":valores.get("edad").toString());
                params.put("latitud", valores.get("latitud").toString());
                params.put("longitud", valores.get("longitud").toString());
                params.put("mujer_id", valores.get("mujer_id")==null?"":valores.get("mujer_id").toString());
                params.put("id", valores.get("id").toString());
                params.put("codigo_vigilante", cod_vigilante);

                return params;

            }
        };

        addToQueue(putRequest);

    }


    public  void EnviarNotificacionMuerteMujer(final VolleyCallbackMuerteMujer callback, NotificacionMuerteMujer notificacion, final String cod_vigilante){

        final ContentValues valores=notificacion.toValues();


        String url = this.path+"/notificar-muerte-mujer";
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{

                            json_data=new JSONObject(response);
                            boolean status=(Boolean) json_data.get("status");



                            callback.onSuccess(json_data,status);


                        }
                        catch (Exception $e){

                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();


                params.put("embarazada",valores.get("embarazada").toString());
                params.put("nombres", valores.get("nombres")==null?"":valores.get("nombres").toString());
                params.put("fecha", valores.get("fecha").toString());
                params.put("apellidos", valores.get("apellidos")==null?"":valores.get("apellidos").toString());
                params.put("direccion", valores.get("direccion")==null?"":valores.get("direccion").toString());
                params.put("telefono", valores.get("telefono")==null?"":valores.get("telefono").toString());
                params.put("edad", valores.get("edad")==null?"0":valores.get("edad").toString());
                params.put("latitud", valores.get("latitud").toString());
                params.put("longitud", valores.get("longitud").toString());
                params.put("mujer_id", valores.get("mujer_id")==null?"":valores.get("mujer_id").toString());
                params.put("id", valores.get("id").toString());
                params.put("codigo_vigilante", cod_vigilante);

                return params;

            }
        };

        addToQueue(putRequest);

    }

    public  void EnviarNotificacionMuerteBebe(final VolleyCallbackMuerteBebe callback, NotificacionMuerteBebe notificacion, final String cod_vigilante){

        final ContentValues valores=notificacion.toValues();


        String url = this.path+"/notificar-muerte-bebe";
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{

                            json_data=new JSONObject(response);
                            boolean status=(Boolean) json_data.get("status");



                            callback.onSuccess(json_data,status);


                        }
                        catch (Exception $e){

                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();

                params.put("id", valores.get("id").toString());
                params.put("nacido",valores.get("nacido").toString());
                params.put("fecha", valores.get("fecha").toString());
                params.put("nombres", valores.get("nombres")==null?"":valores.get("nombres").toString());
                params.put("apellidos", valores.get("apellidos")==null?"":valores.get("apellidos").toString());
                params.put("direccion", valores.get("direccion")==null?"":valores.get("direccion").toString());
                params.put("telefono", valores.get("telefono")==null?"":valores.get("telefono").toString());
                params.put("edad", valores.get("edad")==null?"0":valores.get("edad").toString());
                params.put("latitud", valores.get("latitud").toString());
                params.put("longitud", valores.get("longitud").toString());
                params.put("mujer_id", valores.get("mujer_id")==null?"":valores.get("mujer_id").toString());
                params.put("codigo_vigilante", cod_vigilante);

                return params;

            }
        };

        addToQueue(putRequest);

    }

    public  void EnviarNotificacionParto(final VolleyCallbackParto callback, NotificacionParto notificacion, final String cod_vigilante){

        final ContentValues valores=notificacion.toValues();


        String url = this.path+"/notificar-parto";
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{

                            json_data=new JSONObject(response);
                            boolean status=(Boolean) json_data.get("status");



                            callback.onSuccess(json_data,status);


                        }
                        catch (Exception $e){

                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();

                params.put("id", valores.get("id").toString());
                params.put("atendido_por",valores.get("atendido_por").toString());
                params.put("fecha", valores.get("fecha").toString());
                params.put("nombres", valores.get("nombres")==null?"":valores.get("nombres").toString());
                params.put("apellidos", valores.get("apellidos")==null?"":valores.get("apellidos").toString());
                params.put("direccion", valores.get("direccion")==null?"":valores.get("direccion").toString());
                params.put("telefono", valores.get("telefono")==null?"":valores.get("telefono").toString());
                params.put("edad", valores.get("edad")==null?"0":valores.get("edad").toString());
                params.put("latitud", valores.get("latitud").toString());
                params.put("longitud", valores.get("longitud").toString());
                params.put("mujer_id", valores.get("mujer_id")==null?"":valores.get("mujer_id").toString());
                params.put("codigo_vigilante", cod_vigilante);

                return params;

            }
        };

        addToQueue(putRequest);

    }


    public void EnviarMujer(final VolleyCallbackMujer  callback, final  Mujer mujer,final String cod_vigilante){
        final ContentValues valores=mujer.toValues();
        String url = this.path+"/registrar-mujer";
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{
                            Log.i("response", "String response: "+response);

                            json_data=new JSONObject(response);
                            boolean status=(Boolean) json_data.get("status");
                            callback.onSuccess(json_data,status);
                        }
                        catch (Exception e){
                            Log.i("response","catch (Exception e)"+e.getMessage()+" response"+response);
                            callback.ErrorHandler(e.getMessage());
//                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("response"," public void onErrorResponse(VolleyError error)"+ error);

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();

//                params.put("foto",valores.get("foto").toString());
                params.put("foto","");
                params.put("nombres", valores.get("nombres").toString());
                params.put("apellidomaterno", valores.get("apellidomaterno")==null?"":valores.get("apellidomaterno").toString());
                params.put("apellidopaterno", valores.get("apellidopaterno")==null?"":valores.get("apellidopaterno").toString());
                params.put("fechanacimiento", valores.get("fechanacimiento").toString());
                params.put("carnet", valores.get("carnet")==null?"":valores.get("carnet").toString());
//                params.put("expedido", valores.get("expedido").toString());
                params.put("expedido", "");
                params.put("direccion", valores.get("direccion").toString());
                params.put("telefono", valores.get("telefono")==null?"":valores.get("telefono").toString());
                params.put("idmujer", valores.get("id").toString());
                params.put("edit", valores.get("edit").toString());
                params.put("codigo_vigilante", cod_vigilante);
                 Log.i("response","PARAMS  ENVIANDO AL WEB SERVICE DES DE DATA CONECTION::"+params.toString()+ "CODIGO DEL VIGILANTE" );

                return params;

            }
        };

        addToQueue(putRequest);

    }
    public  void BuscarMujerApi(final VolleyCallbackBuscarMujerApi callback,String numcarnet,String fechanacimiento,String complemento){


        String url = this.path+"/personas/buscar?num_carnet="+numcarnet+"&fecha_nacimiento="+fechanacimiento+"&complemento="+complemento;
        StringRequest putRequest=new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{
                            Log.i("response", "String response: "+response);

                            json_data=new JSONObject(response);
//                            boolean status=(Boolean) json_data.get("status");
                            callback.onSuccess(json_data,true);
                        }
                        catch (Exception e){
                            Log.i("response","catch (Exception e)"+e.getMessage()+" response"+response);
                            callback.ErrorHandler(e.getMessage());
//                            callback.onSuccess(null,false);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("response"," public void onErrorResponse(VolleyError error)"+ error);

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();
                 return params;

            }
        };

        addToQueue(putRequest);
    }

    public  void ApiStatus(final VolleyCallbackApiStatus callback){


        String url = this.path_sus+"/status";
        StringRequest putRequest=new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{
//                            Log.i("response", "String response: "+response);

                            json_data=new JSONObject(response);
                            callback.onSuccess(json_data,true);
                        }
                        catch (Exception e){
                            Log.i("response","catch (Exception e)"+e.getMessage()+" response"+response);
                            callback.ErrorHandler(e.getMessage());
//                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("response"," public void onErrorResponse(VolleyError error)"+ error);

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  =new HashMap<>();
                return params;

            }
        };

        addToQueue(putRequest);
    }
    public void RecuperarData(final recuperarDataCallback callback,  final String cod_telefono){
        String url = this.path+"/recuperar-data";

        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{

                            json_data=new JSONObject(response);

                            boolean status=(Boolean) json_data.get("status");

                            callback.onSuccess(json_data,status);
                        }
                        catch (Exception e){

                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  = new HashMap<>();
                params.put("codigo",cod_telefono);
                return params;

            }
        };

        addToQueue(putRequest);
    }

    public void SyncData(final SyncDataCallback callback, final HashMap<String,JsonArray> datos, final String cod_telefono){
        String url = this.path+"/sync-data";
        Log.d("url",url);
        StringRequest putRequest=new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{

                            json_data=new JSONObject(response);
                            Log.i("response","datos de pai::>"+json_data.toString());
                            boolean status=(Boolean) json_data.get("status");

                            callback.onSuccess(json_data,status);
                        }
                        catch (Exception e){

                            callback.onSuccess(null,false);
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.ErrorHandler(handlerError(error));

            }

        }

        ){

            @Override
            protected Map<String, String>getParams(){

                Map<String, String> params  = new HashMap<>();


                params.put("mujeres",datos.get("mujeres").toString());
                params.put("notificacion_embarazo",datos.get("notificacion_embarazo").toString());
                params.put("notificacion_muerte_mujer",datos.get("notificacion_muerte_mujer").toString());
                params.put("notificacion_muerte_bebe",datos.get("notificacion_muerte_bebe").toString());
                params.put("notificacion_parto",datos.get("notificacion_parto").toString());

                params.put("codigo",cod_telefono);

                return params;

            }
        };

        addToQueue(putRequest);
    }


//    INTERFACES

    public interface VolleyCallback{
        void onSuccessNotificacion(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }

    public interface VolleyCallbackActivar{
        void onSuccessNotificacion(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }

    public interface VolleyCallbackMujer{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface SyncDataCallback{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface recuperarDataCallback{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface VolleyCallbackMuerteMujer{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }

    public interface VolleyCallbackMuerteBebe{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface VolleyCallbackParto{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface VolleyCallbackBuscarMujerApi{
        void onSuccess(JSONObject result,Boolean res);

        void ErrorHandler(String error);
    }
    public interface VolleyCallbackApiStatus{
        void onSuccess(JSONObject result, Boolean res);

        void ErrorHandler(String error);
    }






}
