package evsoft.deadcode.com.sveci.Clases;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


public class AccessLocation  extends AppCompatActivity{

private static final int GPS_ENABLE_REQUEST = 0x1001;
private AlertDialog mGPSDialog;




    public AccessLocation() {


    }

    public boolean tienePermisoGps(Context context) {
        boolean result = false;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {

        result=false;

        } else {
            result=true;

        }
        return result;
    }

    public void mostrarMensajePermiso(Activity activity){
                      Toast.makeText(activity.getApplicationContext(),"Activar GPS",Toast.LENGTH_SHORT).show();

        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        }, 1);
    }

    public void mostrarMensajeActivacionGPS(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("LA UBICACION ESTA DESACTIVADA");
        builder.setMessage("Ubicacion GPS esta desactivada porfavor active la ubicacion para poder enviar correctamente la informacion");
        builder.setPositiveButton("ACTIVAR UBICACION", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_ENABLE_REQUEST);
            }
        }).setNegativeButton("SALIR ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mGPSDialog = builder.create();
        mGPSDialog.show();
    }

    public Boolean isActiveGPS(){
        Boolean result=false;

        LocationManager locationManager= (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

          result=true;
        }
        else{
           result=false;
        }

        return result;
    }



}


