package evsoft.deadcode.com.sveci.Clases;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.Executor;

import evsoft.deadcode.com.sveci.NotificarMujerEmbarazadaActivity;

public class GPSTracker extends ActivityCompat implements LocationListener {

    Context context;
    Activity activity;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;

    public Location location;


    public GPSTracker(Context context, Activity activity) {

    this.context=context;
    activity=activity;

    }

    public Location getLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
            return null;
        }
        else{
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Boolean isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);

            Location location=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            return location;
        }
        else {
            Toast.makeText(context,"Habilitar GPS",Toast.LENGTH_SHORT).show();
            return null;

        }
         }


    }

    @SuppressLint("MissingPermission")
    public Boolean isGPSEnabled(){
        Boolean result=false;
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Boolean isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 6000, 10, this);

            Location location=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
           result=true;
        }
        else {
            Toast.makeText(context,"Habilitar GPS",Toast.LENGTH_SHORT).show();
            result= false;

        }

        return  result;

    }
    public Location obtenerCoordenadas(){

        try {
            final Location[] loc = {null};
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(5000);
            locationRequest.setFastestInterval(1000);
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                              loc[0] = location;
                            }

                        }
                    });


              return loc[0];
        }
        catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public void requestPermissions(Activity a) {
        Toast.makeText(context,"shouldProvideRationale",Toast.LENGTH_SHORT).show();

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(a,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
//
        if (shouldProvideRationale) {
            Toast.makeText(context,"shouldProvideRationale",Toast.LENGTH_SHORT).show();


            startLocationPermissionRequest();

//            showSnackbar(R.string.permisos_gps, android.R.string.ok,
//                    new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            // Request permission
//                            startLocationPermissionRequest();
//                        }
//                    });

        } else {
            Toast.makeText(context,"Habilitar GPS CARAJO",Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(activity, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, 1);


//            startLocationPermissionRequest();
        }
    }

    public  boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
//        mFusedLocationClient.requestLocationUpdates(locationRequest,mLocationCallback, null);
    }
//    @SuppressWarnings("MissingPermission")

//    private void getLastLocation() {
//        mFusedLocationClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
//            @Override
//            public void onComplete(@NonNull Task<Location> task) {
//                if (task.isSuccessful() && task.getResult() != null) {
//                    LolastLocation = task.getResult();
//
//                    Toast.makeText(activity,String.valueOf(lastLocation.getLongitude()),Toast.LENGTH_SHORT).show();
//
//
//                } else {
////                            Log.w(TAG, "getLastLocation:exception", task.getException());
////                            showSnackbar(getString(R.string.permisos_gps));
//                }
//            }
//        });
//    }




    @Override
    public void onLocationChanged(Location location) {

//        Toast.makeText(context,String.valueOf(location.getLatitude()),Toast.LENGTH_SHORT).show();
             this.location=location;

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
//        Toast.makeText(context,"onStatusChanged",Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onProviderEnabled(String s) {
//        Toast.makeText(context,"GPS Habilitado",Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onProviderDisabled(String s) {
//        Toast.makeText(context,"Habilite el GPS",Toast.LENGTH_SHORT).show();


    }
}
