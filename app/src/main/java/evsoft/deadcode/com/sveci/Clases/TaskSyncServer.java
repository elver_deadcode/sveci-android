package evsoft.deadcode.com.sveci.Clases;


import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import evsoft.deadcode.com.sveci.MainActivity;
import evsoft.deadcode.com.sveci.R;

public class TaskSyncServer extends AsyncTask<Context,Integer,Void>
{

    private static final String CHANNEL_ID ="000016666" ;
    private BroadcastReceiver mNetworkReceiver;

    Database database;


   @Override
   protected void onPreExecute(){

       super.onPreExecute();


   }


    @Override
    protected Void doInBackground(Context... contexts) {

         final Context context=contexts[0];
               SharedPreferences sharedPreferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);

       final String codigo = sharedPreferences.getString("codigo", "");

        database=new Database(context);

        try {

            JsonArray array_mujeres=database.MujeresSync();
            JsonArray array_noti_embarazo=database.NotificacionEmbarazoSync();
            JsonArray array_noti_muerte_mujer=database.NotificacionMuerteMujerSync();
            JsonArray array_noti_muerte_bebe=database.NotificacionMuerteBebeSync();
            JsonArray array_noti_parto=database.NotificacionPartoSync();


         if(array_mujeres.size()>0 || array_noti_embarazo.size()>0 || array_noti_muerte_mujer.size()>0 || array_noti_muerte_bebe.size()>0 || array_noti_parto.size()>0)
         {
//             Toast.makeText(context, "hay datios que enviar", Toast.LENGTH_SHORT).show();
             HashMap<String, JsonArray> data = new HashMap<>();
             data.put("mujeres", array_mujeres);
             data.put("notificacion_embarazo", array_noti_embarazo);
             data.put("notificacion_muerte_mujer", array_noti_muerte_mujer);
             data.put("notificacion_muerte_bebe", array_noti_muerte_bebe);
             data.put("notificacion_parto", array_noti_parto);


             Log.d("json", "lista de mujeres" + array_mujeres.toString());
             Log.d("json", "lista de embarazo" + array_noti_embarazo.toString());
             Log.d("json", "lista de muerte mujer" + array_noti_muerte_mujer.toString());
             Log.d("json", "lista de muerte bebe" + array_noti_muerte_bebe.toString());
             Log.d("json", "lista de parto" + array_noti_parto.toString());

             DataConnection dataConnection = new DataConnection(context);


             dataConnection.SyncData(new DataConnection.SyncDataCallback() {
                 @Override
                 public void onSuccess(JSONObject response, Boolean res) {

                     if (res == true) {
                         JSONArray jsonmujeres_array = new JSONArray();
                         JSONArray response_notificacion_embarazo = new JSONArray();
                         JSONArray response_noti_muerte_mujer = new JSONArray();
                         JSONArray response_noti_muerte_bebe = new JSONArray();
                         JSONArray response_noti_parto = new JSONArray();

                         try {
                             jsonmujeres_array = response.getJSONArray("response_mujeres");
                             response_notificacion_embarazo = response.getJSONArray("response_notificacion_embarazo");
                             response_noti_muerte_mujer = response.getJSONArray("response_noti_muerte_mujer");
                             response_noti_muerte_bebe = response.getJSONArray("response_noti_muerte_bebe");
                             response_noti_parto = response.getJSONArray("response_noti_parto");

                             if (jsonmujeres_array.length() > 0) {
                                 actualizar_mujeres(jsonmujeres_array);
                             }
                             if (response_notificacion_embarazo.length() > 0) {

                                 actualizarEmbarazo(response_notificacion_embarazo);
                             }

                             if (response_noti_muerte_mujer.length() > 0) {
                                 actualizarMuerteMujer(response_noti_muerte_mujer);
                             }
                             if (response_noti_muerte_bebe.length() > 0) {

                                 actualizarMuerteBebe(response_noti_muerte_bebe);
                             }
                             if (response_noti_parto.length() > 0) {
                                 actualizarParto(response_noti_parto);
                             }


                         } catch (Exception e) {
                             jsonmujeres_array = null;
                             Log.i("response", "error " + e.getMessage());

                         }

                     }
                     notificar(context);
                 }


                 @Override
                 public void ErrorHandler(String error) {

                 }
             }, data, codigo);


         }


        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        cancel(true);
        return null;

    }

    @Override
    protected void onProgressUpdate(Integer... values){
       super.onProgressUpdate();

    }

    @Override
    protected void onPostExecute(Void aVoid){
       super.onPostExecute(aVoid);
    }

    @Override
    protected  void onCancelled(){
       super.onCancelled();
        Log.i("tiempo", "task terminado" );
//        Toast.makeText(getApplicationContext(), "service done", Toast.LENGTH_SHORT).show();
    }





    //FUNCIONES DEL TASK

    protected  void notificar(Context context){

                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if(alarmSound == null){
            alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            if(alarmSound == null){
                alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }
        }
        long[] vibrate = { 150, 300, 150, 300 };


        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.iconoapp)
                .setContentTitle("SVEC")
                .setContentText("En buena hora!!! Se ha sincronizado la informacion de su celular con el centro de salud. ")
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
//                .setSound(alarmSound)
                .setVibrate(vibrate)


                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(666, mBuilder.build());


    }


    protected void actualizar_mujeres(JSONArray jsonmujeres_array){

        try {
            for (int i = 0; i < jsonmujeres_array.length(); i++) {
                JSONObject o = jsonmujeres_array.getJSONObject(i);
                Boolean status = o.getBoolean("status");
                if (status == false) {
                    Log.i("response", "mujer con error: " + jsonmujeres_array.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    String mensaje = o.getString("mensaje");
                    String id = m.getString("idAndroid");
                    Log.i("response", "IDANDROID: " +id+" ERROR="+mensaje);

                    database.updateErrorMujer(id,mensaje);

                } else {
                    Log.i("response", "mujer guardo en el servidor: " + jsonmujeres_array.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    Log.i("response", "data-mujer: " + m.toString());
                    String id = m.getString("IdAndroid");
                    Log.i("response", "id:::: " + id);
                    database.updateSyncApiMujer(id, 1);


                }

            }
        }
        catch (Exception e){

        }
    }

    protected void actualizarEmbarazo(JSONArray response_notificacion_embarazo){

        try {
            for (int i = 0; i < response_notificacion_embarazo.length(); i++) {
                JSONObject o = response_notificacion_embarazo.getJSONObject(i);
                Boolean status = o.getBoolean("status");


                if (status == false) {
                    String error  = o.getString("mensaje");
                    JSONObject m = o.getJSONObject("data");
                    String id = m.getString("IdAndroid");
                    Log.i("response", "ERROR: " +error+"::idAndroid="+id+"::"+ response_notificacion_embarazo.get(i).toString());
                    database.updateErrorApiNotificacion("notificaciones_embarazo",id, error,"no enviado");

                } else {
                    String evento=o.getString("evento");
                    int sync=0;
                    String estado="enviado";
                    Log.i("response", "STATUS " +status);
                    Log.i("response", "embarazo: " + response_notificacion_embarazo.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    Log.i("response", "data-embarazo: " + m.toString());
                    String id = m.getString("IdAndroid");
                    int id_api = m.getInt("IdNotificacionEmbarazo");
                    Log.i("response", "id:::: " + id);
                    switch(evento){
                        case "inserto": sync=1;break;
                        case "visto": sync=2; estado="visto";break;
                        case "anulado": sync=3; estado="anulado"; break;
                    }
                    database.updateSyncApiEmbarazo(id, sync,id_api,estado);
                    Log.i("response", "se actualizo ");

                }

            }
        }
        catch (Exception e){

        }
    }

    protected void actualizarMuerteMujer(JSONArray response_noti_muerte_mujer){

        try {
            for (int i = 0; i < response_noti_muerte_mujer.length(); i++) {
                JSONObject o = response_noti_muerte_mujer.getJSONObject(i);
                Boolean status = o.getBoolean("status");

                if (status == false) {
                    String error  = o.getString("mensaje");
                    JSONObject m = o.getJSONObject("data");
                    String id = m.getString("IdAndroid");
                    database.updateErrorApiNotificacion("notificaciones_muerte_mujer",id, error,"no enviado");
                    Log.i("response", "se actualizo el error ");

                    Log.i("response", "muerte mujer: " + response_noti_muerte_mujer.get(i).toString());

                } else {
                    String evento=o.getString("evento");
                    int sync=0;
                    String estado="enviado";
                    Log.i("response", "muerte mujer: " + response_noti_muerte_mujer.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    Log.i("response", "data-muerte mujer: " + m.toString());
                    String id = m.getString("IdAndroid");
                    int id_api = m.getInt("IdNotificacionMuerteMujer");
                    Log.i("response", "id:::: " + id);
                    switch(evento){
                        case "inserto": sync=1;break;
                        case "visto": sync=2; estado="visto";break;
                        case "anulado": sync=3; estado="anulado"; break;
                    }
                    database.updateSyncApiMuerteMujer(id, sync,id_api,estado);
                    Log.i("response", "se actualizo ");

                }

            }
        }
        catch (Exception e){

            Log.i("response", "actualizarMuerteMujer-errror: "+e.getMessage());

        }
    }


    protected void actualizarMuerteBebe(JSONArray response_noti_muerte_bebe){

        try {

            for (int i = 0; i < response_noti_muerte_bebe.length(); i++) {
                JSONObject o = response_noti_muerte_bebe.getJSONObject(i);

                Boolean status = o.getBoolean("status");

                if (status == false) {

                    String error  = o.getString("mensaje");
                    JSONObject m = o.getJSONObject("data");
                    String id = m.getString("IdAndroid");
                    database.updateErrorApiNotificacion("notificaciones_muerte_bebe",id, error,"no enviado");

                } else {
                    String evento=o.getString("evento");
                    int sync=0;
                    String estado="enviado";
                    Log.i("response", "muerte bebe: " + response_noti_muerte_bebe.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    Log.i("response", "data-muerte bebe data: " + m.toString());
                    String id = m.getString("IdAndroid");
                    int id_api = m.getInt("IdNotificacionMuerteBebe");
                    Log.i("response", "id:::: " + id);
                    switch(evento){
                        case "inserto": sync=1;break;
                        case "visto": sync=2; estado="visto";break;
                        case "anulado": sync=3; estado="anulado"; break;
                    }
                    database.updateSyncApiMuerteBebe(id, sync,id_api,estado);
                    Log.i("response", "se actualizo ");

                }

            }
        }
        catch (Exception e){
            Log.i("error", e.getMessage());
        }
    }

    protected void actualizarParto(JSONArray response_noti_parto){

        try {
            for (int i = 0; i < response_noti_parto.length(); i++) {
                JSONObject o = response_noti_parto.getJSONObject(i);
                Boolean status = o.getBoolean("status");

                if (status == false) {

                    String error  = o.getString("mensaje");
                    JSONObject m = o.getJSONObject("data");
                    String id = m.getString("IdAndroid");
                    database.updateErrorApiNotificacion("notificaciones_parto",id, error,"no enviado");

                } else {
                    String evento=o.getString("evento");
                    int sync=0;
                    String estado="enviado";
                    Log.i("response", "parto: " + response_noti_parto.get(i).toString());
                    JSONObject m = o.getJSONObject("data");
                    Log.i("response", "data-parto: " + m.toString());
                    String id = m.getString("IdAndroid");
                    int id_api = m.getInt("IdNotificacionParto");
                    Log.i("response", "id:::: " + id);
                    switch(evento){
                        case "inserto": sync=1;break;
                        case "visto": sync=2; estado="visto";break;
                        case "anulado": sync=3; estado="anulado"; break;
                    }
                    database.updateSyncApiParto(id, sync,id_api,estado);
                    Log.i("response", "se actualizo ");

                }

            }
        }
        catch (Exception e){

        }
    }

}
