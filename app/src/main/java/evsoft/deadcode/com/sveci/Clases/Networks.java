package evsoft.deadcode.com.sveci.Clases;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;

public class Networks extends AppCompatActivity {

    private Activity activity;

    public Networks(Activity activity) {
        this.activity = activity;
    }

    public Boolean verificarConexion(){
        boolean result=false;
        ConnectivityManager  connectivityManager=(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo!=null){
            if(networkInfo.getType()==ConnectivityManager.TYPE_WIFI){
                result=true;
            }
            else if(networkInfo.getType()==ConnectivityManager.TYPE_MOBILE){
                    result=true;
            }

        }
        else {
            result=false;
        }
        return  result;


    }

}
