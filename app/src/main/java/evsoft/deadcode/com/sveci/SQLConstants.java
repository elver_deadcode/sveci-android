package evsoft.deadcode.com.sveci;

public class SQLConstants {

    public static final String DB_NAME="dbsveci.db";

    public static final String DB_TABLE_MUJER="mujeres";
    public static final String DB_TABLE_NOTIFICACIONES_EMBARZAZO="notificaciones_embarazo";
    public static final String DB_TABLE_NOTIFICACIONES_MUERTE_MUJER="notificaciones_muerte_mujer";
    public static final String DB_TABLE_NOTIFICACIONES_MUERTE_BEBE="notificaciones_muerte_bebe";
    public static final String DB_TABLE_NOTIFICACIONES_PARTO="notificaciones_parto";

    public static final String DB_CREATE_TABLE_MUJER=
            "CREATE TABLE "+DB_TABLE_MUJER+"("+
            "id TEXT NOT NULL PRIMARY KEY , " +
            " nombres TEXT, " +
            "apellidoPaterno TEXT, " +
            "apellidoMaterno TEXT, " +
            "fechanacimiento TEXT, " +
            "direccion TEXT, " +
            "carnet TEXT, " +
            "expedido TEXT, " +
            "sync integer, " +
            "edit integer, " +
            "foto TEXT, " +
            "error TEXT, " +
            "telefono TEXT, " +
            "adscrito integer, " +
            "complemento TEXT" +
            " ); ";

    public static final String DB_CREATE_TABLE_NOTIFICACIONES_EMBARAZO=
            "CREATE TABLE "+ DB_TABLE_NOTIFICACIONES_EMBARZAZO+" ("+
            "id TEXT NOT NULL PRIMARY KEY , "+
            "tipo TEXT,"+
            "fecha TEXT,"+
            "sync integer,"+
            "edit integer,"+
            "nombres TEXT,"+
            "estado TEXT,"+
            "error TEXT,"+
            "apellidos TEXT,"+
            "direccion TEXT,"+
            "telefono INTEGER,"+
            "edad integer,"+
            "latitud real,"+
            "longitud real,"+
            "mujer_id TEXT,"+
            "id_api INTEGER"+
            ");";

    public static final String DB_CREATE_TABLE_NOTIFICACIONES_MUERTE_MUJER=
            "CREATE TABLE "+ DB_TABLE_NOTIFICACIONES_MUERTE_MUJER+" ("+
                    "id TEXT NOT NULL PRIMARY KEY , "+
                    "embarazada integer,"+
                    "fecha TEXT,"+
                    "sync integer,"+
                    "edit integer,"+
                    "nombres TEXT,"+
                    "estado TEXT,"+
                    "error TEXT,"+
                    "apellidos TEXT,"+
                    "direccion TEXT,"+
                    "telefono INTEGER,"+
                    "edad integer,"+
                    "latitud real,"+
                    "longitud real,"+
                    "mujer_id TEXT,"+
                    "id_api INTEGER"+
                    ");";

    public static final String DB_CREATE_TABLE_NOTIFICACIONES_MUERTE_BEBE=
            "CREATE TABLE "+ DB_TABLE_NOTIFICACIONES_MUERTE_BEBE+" ("+
                    "id TEXT NOT NULL PRIMARY KEY , "+
                    "nacido integer,"+
                    "fecha TEXT,"+
                    "sync integer,"+
                    "edit integer,"+
                    "nombres TEXT,"+
                    "estado TEXT,"+
                    "error TEXT,"+
                    "apellidos TEXT,"+
                    "direccion TEXT,"+
                    "telefono INTEGER,"+
                    "edad integer,"+
                    "latitud real,"+
                    "longitud real,"+
                    "mujer_id TEXT,"+
                    "id_api INTEGER"+
                    ");";

    public static final String DB_CREATE_TABLE_NOTIFICACIONES_PARTO=
            "CREATE TABLE "+ DB_TABLE_NOTIFICACIONES_PARTO+" ("+
                    "id TEXT NOT NULL PRIMARY KEY , "+
                    "partera TEXT,"+
                    "atendido_por TEXT,"+
                    "fecha TEXT,"+
                    "sync integer,"+
                    "edit integer,"+
                    "nombres TEXT,"+
                    "estado TEXT,"+
                    "error TEXT,"+
                    "apellidos TEXT,"+
                    "direccion TEXT,"+
                    "telefono INTEGER,"+
                    "edad integer,"+
                    "latitud real,"+
                    "longitud real,"+
                    "mujer_id TEXT,"+
                    "id_api INTEGER"+
                    ");";

    public static final String DB_DROP_TABLE_NOTIFICACIONES="DROP TABLE EXISTS "+DB_TABLE_NOTIFICACIONES_EMBARZAZO;
    public static final String DB_DROP_TABLE_NOTIFICACIONES_MUERTE_MUJER="DROP TABLE EXISTS "+DB_TABLE_NOTIFICACIONES_MUERTE_MUJER;
    public static final String DB_DROP_TABLE_NOTIFICACIONES_MUERTE_BEBE="DROP TABLE EXISTS "+DB_TABLE_NOTIFICACIONES_MUERTE_BEBE;
    public static final String DB_DROP_TABLE_NOTIFICACIONES_PARTO="DROP TABLE EXISTS "+DB_TABLE_NOTIFICACIONES_PARTO;
    public static final String DB_DROP_TABLE_MUJER="DROP TABLE EXISTS "+DB_TABLE_MUJER;



}
