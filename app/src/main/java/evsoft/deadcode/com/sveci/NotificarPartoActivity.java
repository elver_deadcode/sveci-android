package evsoft.deadcode.com.sveci;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.GPSTracker;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Clases.ValidateHelper;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteBebe;
import evsoft.deadcode.com.sveci.model.NotificacionParto;

public class NotificarPartoActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 1;

    private ValidateHelper validator;


    private TextInputLayout ly_notiPartoNombres;
    private TextView error_spinner;
    private Button btn_notificar;
    private FloatingActionButton btn_buscar;
    private Toolbar toolbar;

    private TextInputEditText txt_nombres;
    private TextInputEditText txt_apellidos;
    private TextInputEditText txt_direccion;
    private Mujer mujer;
    private Networks network;
    private Button btn_limpiar;

    private int nacido=1;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    private Boolean isActiveGps=false;
    private GPSTracker gpsTracker;
    private Location location;
    private DataConnection webservice;
    private Database database;

    private Spinner spinner;
    private boolean selected=false;
    private String atendido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificar_parto);

        initActivity();
        showToolbar("NOTIFICAR PARTO DE MUJER",true);
        initiListeners();

        gpsTracker=new GPSTracker(getApplicationContext(),this);

        if (!gpsTracker.checkPermissions()) {
            requestPermissions();
        } else {
            this.location=gpsTracker.getLocation();
        }

        Log.i("hora", "onCreate: "+ Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"));

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private Boolean checkValidation(){

        if (!validator.isEditTextFilled(txt_nombres, ly_notiPartoNombres, "Error: Tienes que escribir un nombre.")) {
            return false;
        }

            if(!selected){

            error_spinner.setVisibility(View.VISIBLE);

            return  false;
            }
            else{

             error_spinner.setVisibility(View.GONE);
            }
        return true;



    }
    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }
    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {

            startLocationPermissionRequest();


        } else {

            startLocationPermissionRequest();
        }
    }


    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
    }
    private void deshabilitarCampos(){
        txt_nombres.setEnabled(false);
        txt_apellidos.setEnabled(false);

    }
    private void initActivity() {

        ly_notiPartoNombres=findViewById(R.id.ly_notiPartoNombres);
        error_spinner=(TextView) findViewById(R.id.error_spinner);


        btn_notificar=(Button) findViewById(R.id.btn_notiparto);
        txt_nombres=(TextInputEditText)findViewById(R.id.txt_notiPartoNombres);

        txt_apellidos=(TextInputEditText)findViewById(R.id.txt_notiPartoApellidos);
        txt_direccion=(TextInputEditText)findViewById(R.id.txt_notiPartoDireccion);

        validator = new ValidateHelper(this);
        deshabilitarCampos();
        spinner = (Spinner) findViewById(R.id.spinner_notiParto);
        List<String> lista = new ArrayList<>();

        lista.add("Selecciona una opcion");
        lista.add("Partera capacitada");
        lista.add("Partera empirica");
        lista.add("Familiar");



        ArrayAdapter<String>  comboAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,lista);

        comboAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(comboAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                atendido = (String) parent.getItemAtPosition(position);



                if(position>0){
                    selected=true;

                    error_spinner.setVisibility(View.GONE);
                }
                else{
                    selected=false;
//                    error_spinner.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        database=new Database(this);
        btn_buscar=(FloatingActionButton) findViewById(R.id.fab_buscarMujerParto);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.orange);

        mujer=null;
        btn_limpiar=(Button) findViewById(R.id.btn_notiPartoLimpiar);
        this.network=new Networks(this);
    }

    private void initiListeners() {


        btn_notificar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                notificar();
            }

        });

        btn_buscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                buscarMujer();
            }

        });

        btn_limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarVista();
            }
        });



    }

    private void buscarMujer(){

        Intent intent=new Intent(this, ListaMujeresActivity.class);
        intent.putExtra("retroceder",true);

        startActivityForResult(intent,REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(REQUEST_CODE==requestCode && resultCode==RESULT_OK ){
            if( data.getExtras()!=null){

                mujer=(Mujer) data.getExtras().getSerializable("mujer");
                btn_limpiar.setVisibility(View.VISIBLE);
                llenarformulario(mujer);

            }

        }
    }

    public void  llenarformulario(Mujer mujer){


        txt_nombres.setText(mujer.getNombres());
        txt_nombres.setEnabled(false);

        txt_apellidos.setText(mujer.getApellidopaterno() +" "+mujer.getApellidomaterno());
        txt_apellidos.setEnabled(false);
        txt_direccion.setText(mujer.getDireccion());
        txt_direccion.setEnabled(false);

    }

    public void limpiarVista(){

        mujer=null;
        txt_nombres.setText("");
        txt_apellidos.setText("");
        txt_direccion.setText("");
//        txt_nombres.setEnabled(true);
//        txt_apellidos.setEnabled(true);
//        txt_direccion.setEnabled(true);
//        txt_nombres.requestFocus();

        btn_limpiar.setVisibility(View.GONE);

    }

    private void enviarWebService(final NotificacionParto notificacion){

        webservice=new DataConnection(this);
        final  String cod_telefono= Helpers.getShared("codigo",this);

        Log.i("notificacion","enviando al web services==>"+notificacion.toValues().toString());


        btn_notificar.setEnabled(false);

        final android.app.AlertDialog dialog=new SpotsDialog(this);
        dialog.show();
        dialog.setMessage("Guardando Notificacion...");


        webservice.EnviarNotificacionParto(
                new DataConnection.VolleyCallbackParto() {
                    @Override
                    public void onSuccess(JSONObject response_json, Boolean status) {

                        try{
                            if(status ) {

                                JSONObject data = response_json.getJSONObject("data");
                                int id_api=data.getInt("IdNotificacionParto");
                                notificacion.setId_api(id_api);
                                notificacion.setSync(1);
                                Log.i("response","datos desde el api==>"+data.toString());

                                guardarBaseDatos(notificacion);
                            }
                            else{
                                String msg=response_json.getString("mensaje");

                                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                                Log.i("status","error");

                            }
                        }
                        catch (Exception e){ }

                        btn_notificar.setEnabled(true);
                        dialog.dismiss();

                    }
                    @Override
                    public void ErrorHandler(String error) {
                        Log.i("debug","void ErrorHandler(String error)"+error);
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                        btn_notificar.setEnabled(true);
                        dialog.dismiss();
                    }


                },
                notificacion,cod_telefono
        );


    }

    private void guardarBaseDatos(NotificacionParto notificacion){



        try{
            Log.i("db","estito se esta guardando"+notificacion.toValues().toString());

            database.insertNotificacionParto(notificacion);

            String msg="La notificacion  se envio a su centro de salud correctamente. Gracias!!!";

            if(notificacion.getSync()==0){
                msg="La notificacion de Parto no se pudo enviar al centro de salud. Sin embargo esta se guardo en su celular y se  enviara una vez se conecte a internet. Gracias!!!";
            }


            android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                    .setTitle("NOTIFICACION EXITOSA")
                    .setMessage(msg)
                    .setPositiveButton("VER HISTORIAL DE NOTIFICACIONES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent=new Intent(NotificarPartoActivity.this,ListaNotificaciones.class);
                            startActivity(intent);
                            finish();

//                                Log.d("MainActivity", "Sending atomic bombs to Jupiter");
                        }
                    })
//                .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                                Log.d("MainActivity", "Aborting mission...");
//                    }
//                })
                    .show();
            dialog.setCanceledOnTouchOutside(false);

        }
        catch (Exception $e){
            Toast.makeText(this,$e.getMessage(),Toast.LENGTH_LONG).show();

        }


    }


    private void notificar(){

//        if( checkValidation()) {

            double longitud = 0.0;
            double latitud = 0.0;



        if (this.location != null) {

            longitud = this.location.getLongitude();
            latitud = this.location.getLatitude();
        }
        else{
            longitud = 0;
            latitud=0;
        }
        if(!selected){

//            error_spinner.setVisibility(View.VISIBLE);

            Helpers.showTooltips(this, spinner, "SELECCIONA A UNA OPCION");
            return;
        }


            String nombres = txt_nombres.getText().toString();
            String apellidos = txt_apellidos.getText().toString();
            String direccion = txt_direccion.getText().toString();

            String mujer_id = "";
            if (this.mujer != null) {
                boolean validate_muerte=database.validarMujertieneNotificacionMuerte(this.mujer);
                boolean validate=database.validarNotificacionMismoAnio(this.mujer,"notificaciones_parto");

                if(validate_muerte){
                    android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                            .setTitle("ERROR")
                            .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE MUERTE REGISTRADA")
                            .show();
                    return;
                }
                if(validate){
                    android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                            .setTitle("ERROR")
                            .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE PARTO REGISTRADA")
                            .show();
                    return;
                }
                mujer_id = this.mujer.getId();


                NotificacionParto notificacion = new NotificacionParto(

                        atendido,
                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
                        0,
                        "no enviado",
                        nombres,
                        apellidos,
                        direccion,
                        "",
                        0,
                        latitud,
                        longitud,
                        mujer_id

                );
                notificacion.setEdit(0);
                notificacion.setPartera("");
                notificacion.setError("");
                notificacion.setEstado("");

                Boolean connectivity = network.verificarConexion();
                if (connectivity) {
                    Log.i("notificacion==>", notificacion.toValues().toString());

                    enviarWebService(notificacion);

                } else {

                    guardarBaseDatos(notificacion);
                }
            }
            else{
                Helpers.showTooltips(this, btn_buscar, "SELECCIONA A UNA MUJER");

//                mujer_id = "";
//
//
//                NotificacionParto notificacion = new NotificacionParto(
//
//                        atendido,
//                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
//                        0,
//                        nombres,
//                        apellidos,
//                        direccion,
//                        "",
//                        0,
//                        latitud,
//                        longitud,
//                        mujer_id
//
//                );
//                notificacion.setEdit(0);
//                notificacion.setPartera("");
//                notificacion.setError("");
//                notificacion.setEstado("");
//
//                Boolean connectivity = network.verificarConexion();
//                if (connectivity) {
//                    Log.i("notificacion==>", notificacion.toValues().toString());
//
//                    enviarWebService(notificacion);
//
//                } else {
//
//                    guardarBaseDatos(notificacion);
//                }

            }

        }





}
