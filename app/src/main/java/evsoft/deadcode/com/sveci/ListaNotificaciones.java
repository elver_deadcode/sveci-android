package evsoft.deadcode.com.sveci;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Clases.SwipeHelper;
import evsoft.deadcode.com.sveci.Clases.SyncService;
import evsoft.deadcode.com.sveci.adapter.ListaNotificacionesAdapterRecyclerView;
import evsoft.deadcode.com.sveci.model.Notificacion;

public class ListaNotificaciones extends AppCompatActivity implements SwipeHelper.RecyclerItemTouchHelperListener {
    private Boolean retroceder=false;
    private Database database;
    private RecyclerView recycler;
    private ArrayList<Notificacion> listaNotificaciones;
    private  ListaNotificacionesAdapterRecyclerView listaNotificacionesAdapterRecyclerView;
    private int position;
    private Notificacion noti_aux;
    private Button btn_enviar_notis;
    private Networks network;
    private Intent intent_service;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notificaciones);
        initActivity();
        initListeners();
        initRecycler();
        showToolbar("NOTIFICACIONES",true);

        ItemTouchHelper.SimpleCallback simpleCallback= new SwipeHelper(0,ItemTouchHelper.LEFT,ListaNotificaciones.this);
        new  ItemTouchHelper(simpleCallback).attachToRecyclerView(recycler);


        //*****funciones para resetar las notificaciones en el celular solo activar en cas de pruebas de sincronizacion con el servidor
//        setSyncNotificaciones();
//        resetMujeres();
        ///*************************

    }

    private void initActivity() {

        Intent i =getIntent();
        this.retroceder=i.getBooleanExtra("retroceder",false);
        database=new Database(this);
         this.btn_enviar_notis= (Button) findViewById(R.id.btnEnviarNotis);



        recycler = findViewById(R.id.listaNotificacionesRecycler);
        listaNotificaciones=getNotificaciones();

//        sv=(SearchView) findViewById(R.id.mSearch);
//        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                //FILTER AS YOU TYPE
////                listaNotificacionesAdapterRecyclerView.getFilter().filter(query);
//
//                return false;
//            }
//        });





    }
    private void enviarNotificaciones(){
        this.network=new Networks(this);


        Boolean esta_iniciado_servicio=isMyServiceRunning(SyncService.class);

        Boolean connectivity = network.verificarConexion();

        if(!esta_iniciado_servicio && connectivity){

            iniciarServicio();
            final android.app.AlertDialog dialog=new SpotsDialog(this);
            dialog.show();
            dialog.setMessage("Enviando Notificaciones...");
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {

                            dialog.dismiss();
                          Intent intent_configuraciones = new Intent(ListaNotificaciones.this,MainActivity.class);
                            startActivity(intent_configuraciones);

                        }
                    },
                    5000
            );
        }
        else{
            AlertDialog dialog = new AlertDialog.Builder(ListaNotificaciones.this, android.R.style.Theme_Holo_Dialog)
                    .setTitle("CONEXION A INTERNET ")
                    .setMessage("PARA PODER ENVIAR  LAS NOTIFICACIONES  NECESITAS ESTAR CONECTADO A  INTERNET")
                    .setNegativeButton("CERRAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();


                        }
                    }).show();
        }


    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    private void iniciarServicio(){
        intent_service=new Intent(this, SyncService.class);


        this.startService(intent_service);
    }

    private void initListeners() {


        btn_enviar_notis.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                enviarNotificaciones();


            }

        });
    }




    public ArrayList<Notificacion> getNotificaciones(){
        ArrayList<Notificacion> notificaciones= database.getNotificaciones();
        return notificaciones;
    }

    private void initRecycler() {
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(linearLayoutManager);
        this.listaNotificacionesAdapterRecyclerView= new ListaNotificacionesAdapterRecyclerView(listaNotificaciones,R.layout.notificacion_item,this);
        recycler.setAdapter(listaNotificacionesAdapterRecyclerView);




//        listaNotificacionesAdapterRecyclerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Notificacion noti =listaNotificaciones.get(recycler.getChildAdapterPosition(view));



//                Mujer mujer=listaMujeres.get(recycler.getChildAdapterPosition(view));
//                Log.i("position",String.valueOf(recycler.getId()));
//                if(retroceder){
//
//                    enviarActivity2(mujer);
//                }
//                else {
//
//                    Intent intent=new Intent(getApplicationContext(), RegistroMujeresActivity.class);
//
//                    intent.putExtra("editar",true);
//                    intent.putExtra("mujer",mujer);
//                    startActivityForResult(intent,REQUEST_CODE);
//                }

//            }
//        });

//        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                //FILTER AS YOU TYPE
//                listaMujeresAdapterRecyclerView.getFilter().filter(query);
//                return false;
//            }
//        });
    }
    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbarNotificaciones);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setSubtitle("Historial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, final int position) {
        try{
              if(viewHolder instanceof ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder)
              {
                  this.position=viewHolder.getAdapterPosition();
               this.noti_aux=listaNotificaciones.get(viewHolder.getAdapterPosition());
               int sync=noti_aux.getSync();
               if(sync==1){
                   recycler.getAdapter().notifyItemChanged(position);


                   Toast.makeText(getApplicationContext(),"NO SE PUEDE BORRAR UNA NOTIFICACION  QUE YA SE ENVIO", Toast.LENGTH_LONG).show();

               }
               else{
                   AlertDialog dialog = new android.app.AlertDialog.Builder(ListaNotificaciones.this, android.R.style.Theme_Holo_Dialog)
                           .setTitle("BORRAR  LA NOTIFICACION")
                           .setMessage("ESTA SEGURO DE BORRAR LA NOTIFICACION?")
                           .setPositiveButton("SI BORRAR", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   recycler.getAdapter().notifyItemChanged(position);
                                   database.borrarNotificacion(noti_aux.tablaNotificacion(),noti_aux.getId());
                                   listaNotificacionesAdapterRecyclerView.removeItem(position);
                               }
                           })
                           .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                                   recycler.getAdapter().notifyItemChanged(position);
                               }
                           }).show();
               }
              }
        }
        catch (Exception e){

            Log.d("POSITION","ERROR::"+e.getMessage());
        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_notificaciones,menu);
//        return true;
//    }

    protected void setSyncNotificaciones(){
        database.setSyncNotificaciones(0);
    }
    protected void resetMujeres(){
        database.setResetRegistrosMujeres(0);
    }
}
