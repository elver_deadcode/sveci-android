package evsoft.deadcode.com.sveci.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import evsoft.deadcode.com.sveci.Clases.CustomFilter;
import evsoft.deadcode.com.sveci.R;
import evsoft.deadcode.com.sveci.model.Mujer;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.github.ivbaranov.mli.MaterialLetterIcon;

public class ListaMujeresAdapterRecyclerView extends RecyclerView.Adapter<ListaMujeresAdapterRecyclerView.MujerViewHolder> implements Filterable, View.OnClickListener {

    public  ArrayList<Mujer> mujeres, filterList;
    private int resource;
    private Activity activity;
    CustomFilter filter;

    private View.OnClickListener listener;

    private ImageView mIconMujer;

    private AdapterView.OnItemClickListener mListener;

    public ListaMujeresAdapterRecyclerView(ArrayList<Mujer> mujeres, int resource, Activity activity) {
        this.mujeres = mujeres;
        this.filterList=mujeres;
        this.resource = resource;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MujerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(resource, parent,false);

        view.setOnClickListener(this);
        return new MujerViewHolder(view,mListener);


    }
    public void updateRecycler(){
        notifyDataSetChanged();
    }
    public void removeItem(int position){
        mujeres.remove(position);
        notifyDataSetChanged();

    }

    @Override
    public void onBindViewHolder(@NonNull MujerViewHolder holder, int position) {
              Mujer mujer=mujeres.get(position);
              holder.nombres.setText(mujer.getNombres());
              holder.apellidoPaterno.setText(mujer.getApellidopaterno());
              holder.apellidoMaterno.setText(mujer.getApellidomaterno());
              holder.telefono.setText(mujer.getTelefono());
              holder.direccion.setText(mujer.getDireccion());
              holder.numCarnet.setText(mujer.getCarnet());
              String edad=String.valueOf(mujer.Edad())+" AÑOS";
              holder.edad.setText(edad);
              holder.inicial.setText(mujer.getInicial());
             if(mujer.getSync()==0){
                 holder.estado.setText("NO ENVIADO");

                 holder.estado.setTextColor(Color.RED);
             }
             else{
                 holder.estado.setText("ENVIADO");
                 holder.estado.setTextColor(Color.GREEN);
             }
             holder.error.setText(mujer.getError());

    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilter(filterList,this);
        }

        return filter;
    }

    @Override
    public int getItemCount() {
        return mujeres.size();
    }

    public void setOnClickListener(View.OnClickListener listener){

        this.listener=listener;
    }

    @Override
    public void onClick(View view) {

        if(this.listener!=null){
            listener.onClick(view);
        }

    }

    public class MujerViewHolder extends RecyclerView.ViewHolder{


        private TextView inicial;
        private TextView nombres;
        private TextView apellidoPaterno;
        private TextView apellidoMaterno;
        private TextView numCarnet;
        private TextView telefono;
        private TextView edad;
        private TextView direccion;
        private TextView estado;
        private TextView error;
        public RelativeLayout layoutABorrar;


        public MujerViewHolder(View itemView, final AdapterView.OnItemClickListener listener ) {
            super(itemView);

//            foto            =(ImageView) itemView.findViewById(R.id.fotoMujerItem);
            nombres         =(TextView) itemView.findViewById(R.id.nombresMujerItem);
            apellidoPaterno =(TextView) itemView.findViewById(R.id.apellidoPaternoMujerItem);
            apellidoMaterno =(TextView) itemView.findViewById(R.id.apellidoMaternoMujerItem);
            numCarnet       =(TextView) itemView.findViewById(R.id.carnetMujerItem);
            telefono        =(TextView) itemView.findViewById(R.id.telefonoMujerItem);
            edad            =(TextView) itemView.findViewById(R.id.edadMujerItem);
            direccion       =(TextView) itemView.findViewById(R.id.direccionMujerItem);
            inicial =(TextView) itemView.findViewById(R.id.itemIconMujer);
            estado =(TextView) itemView.findViewById(R.id.estadoMujerItem);
            error =(TextView) itemView.findViewById(R.id.errorMujerItem);
            layoutABorrar=(RelativeLayout)itemView.findViewById(R.id.layoutABorrarMujer);

        }
    }
}
