package evsoft.deadcode.com.sveci;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Clases.SyncService;


public class SplashActivity extends AppCompatActivity {
    private static final long SCREEN_SPLASH_TIME=2500;
    private Intent intent_service;
    private Networks network;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio_splash);
        this.network=new Networks(this);


        Boolean esta_iniciado_servicio=isMyServiceRunning(SyncService.class);

        Boolean connectivity = network.verificarConexion();

        if(!esta_iniciado_servicio && connectivity){

            iniciarServicio();
        }



        Boolean isRegister = false;
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
//        sharedPreferences.edit().clear().commit();

        final String codigo = sharedPreferences.getString("codigo", "");
        final String UrlApi=sharedPreferences.getString("url","");

        if(UrlApi.isEmpty()){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url",this.getString(R.string.ApiPath));
            editor.commit();
        }


            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {

                    if (codigo.isEmpty()) {


                        Intent intent = new Intent(SplashActivity.this,ActiveActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(timerTask, SCREEN_SPLASH_TIME);
        }

    private void iniciarServicio(){
        intent_service=new Intent(this, SyncService.class);


        this.startService(intent_service);
    }

    private void pararServicio(){


        this.stopService(intent_service);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
