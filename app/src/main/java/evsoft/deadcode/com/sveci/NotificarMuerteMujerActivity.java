package evsoft.deadcode.com.sveci;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONObject;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.GPSTracker;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.NotificacionEmbarazada;
import evsoft.deadcode.com.sveci.model.NotificacionMuerteMujer;

public class NotificarMuerteMujerActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 1;
    private Button btn_notificar;
    private FloatingActionButton btn_buscar;
    private Toolbar toolbar;

    private TextInputEditText txt_nombres;
    private TextInputEditText txt_apellidos;
    private TextInputEditText txt_edad;
    private Mujer mujer;
    private Networks network;
    private Button btn_limpiar;

    private int esta_embarazada = 0;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    private Boolean isActiveGps = false;
    private GPSTracker gpsTracker;
    private Location location;
    private DataConnection webservice;
    private Database database;
    private RadioButton rb_mujerembarazada;
    private RadioButton rb_mujernoembarazada;
    private RadioButton rb_noconozco;
    private RadioGroup radio_group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificar_muerte_mujer);
        initActivity();
        showToolbar("NOTIFICAR MUERTE DE MUJER", true);
        initiListeners();
        gpsTracker = new GPSTracker(getApplicationContext(), this);

        if (!gpsTracker.checkPermissions()) {
            requestPermissions();
        } else {
            this.location = gpsTracker.getLocation();
        }


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void deshabilitarCampos() {
        txt_nombres.setEnabled(false);
        txt_apellidos.setEnabled(false);
        txt_edad.setEnabled(false);
    }

    public void showToolbar(String titulo, boolean upButton) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {

            startLocationPermissionRequest();


        } else {

            startLocationPermissionRequest();
        }
    }


    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
    }

    private void initActivity() {

        btn_notificar = (Button) findViewById(R.id.btn_notificar_muerte_mujer);
        txt_nombres = (TextInputEditText) findViewById(R.id.txt_notiMuerteMujerNombres);
        txt_apellidos = (TextInputEditText) findViewById(R.id.txt_notiMuerteMujerApellidos);
        txt_edad = (TextInputEditText) findViewById(R.id.txt_notiMuerteMujerEdad);
        rb_mujerembarazada = (RadioButton) findViewById(R.id.rb_notiMujerEmbarazada);
        rb_mujernoembarazada = (RadioButton) findViewById(R.id.rb_notiMujerNoEmbarazada);
        rb_noconozco = (RadioButton) findViewById(R.id.rb_notiMujerNoConozco);
        radio_group = (RadioGroup) findViewById(R.id.rb_group_muertemujer);

        database = new Database(this);


        btn_buscar = (FloatingActionButton) findViewById(R.id.fab_buscar_muerte_mujer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.negro);


        mujer = null;

        btn_limpiar = (Button) findViewById(R.id.btn_notiMuerteMujerLimpiar);

        this.network = new Networks(this);
        deshabilitarCampos();


    }

    private void initiListeners() {


        btn_notificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificar();
            }

        });

        btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarMujer();
            }

        });

        btn_limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarVista();
            }
        });


        rb_mujerembarazada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rb_mujerembarazada.isChecked()) {

                    esta_embarazada = 1;
                }

            }
        });
        rb_mujernoembarazada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rb_mujernoembarazada.isChecked()) {

                    esta_embarazada = 0;
                }

            }
        });
        rb_noconozco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rb_noconozco.isChecked()) {

                    esta_embarazada = 0;
                }

            }
        });

    }

    private void buscarMujer() {

        Intent intent = new Intent(this, ListaMujeresActivity.class);
        intent.putExtra("retroceder", true);

        startActivityForResult(intent, REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (REQUEST_CODE == requestCode && resultCode == RESULT_OK) {
            if (data.getExtras() != null) {

                mujer = (Mujer) data.getExtras().getSerializable("mujer");
                btn_limpiar.setVisibility(View.VISIBLE);
                llenarformulario(mujer);

            }

        }
    }

    public void llenarformulario(Mujer mujer) {


        txt_nombres.setText(mujer.getNombres());
        txt_nombres.setEnabled(false);

        txt_apellidos.setText(mujer.getApellidopaterno() + " " + mujer.getApellidomaterno());
        txt_apellidos.setEnabled(false);
        txt_edad.setText(String.valueOf(mujer.Edad()));
        txt_edad.setEnabled(false);

    }

    public void limpiarVista() {

        mujer = null;
        txt_nombres.setText("");
        txt_apellidos.setText("");
        txt_edad.setText("");

        btn_limpiar.setVisibility(View.GONE);

    }

    private void enviarWebService(final NotificacionMuerteMujer notificacion) {

        webservice = new DataConnection(this);
        final String cod_telefono = Helpers.getShared("codigo", this);

        Log.i("notificacion", "enviando al web services" + notificacion.toValues().toString());


        btn_notificar.setEnabled(false);

        final android.app.AlertDialog dialog = new SpotsDialog(this);
        dialog.show();
        dialog.setMessage("Guardando Notificacion...");


        webservice.EnviarNotificacionMuerteMujer(
                new DataConnection.VolleyCallbackMuerteMujer() {
                    @Override
                    public void onSuccess(JSONObject response_json, Boolean status) {

                        try {
                            if (status) {

                                JSONObject data = response_json.getJSONObject("data");
                                int id_api = data.getInt("IdNotificacionMuerteMujer");
                                notificacion.setId_api(id_api);
                                notificacion.setSync(1);
                                notificacion.setEstado("enviado");

                                guardarBaseDatos(notificacion);

//                            Log.i("response","response="+data);
                                Log.i("response", "notificaion=" + notificacion.toValues());


                            } else {
                                String msg = response_json.getString("mensaje");

                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                Log.i("status", "error");

                            }
                        } catch (Exception e) {
                        }

                        btn_notificar.setEnabled(true);
                        dialog.dismiss();

                    }

                    @Override
                    public void ErrorHandler(String error) {
                        Log.i("debug", "void ErrorHandler(String error)" + error);
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        btn_notificar.setEnabled(true);
                        dialog.dismiss();
                    }


                },
                notificacion, cod_telefono
        );


    }

    private void guardarBaseDatos(NotificacionMuerteMujer notificacion) {


        try {

            database.insertNotificacionMuerteMujer(notificacion);

            String msg = "La notificacion  se envio a su centro de salud correctamente. Gracias!!!";

            if (notificacion.getSync() == 0) {
                msg = "La notificacion de Mujer Embarazada no se pudo enviar al centro de salud. Sin embargo esta se guardo en su celular y se  enviara una vez se conecte a internet. Gracias!!!";
            }


            android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog)
                    .setTitle("NOTIFICACION EXITOSA")
                    .setMessage(msg)
                    .setPositiveButton("VER HISTORIAL DE NOTIFICACIONES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(NotificarMuerteMujerActivity.this, ListaNotificaciones.class);
                            startActivity(intent);
                            finish();

                        }
                    })

                    .show();
            dialog.setCanceledOnTouchOutside(false);

        } catch (Exception $e) {
            Toast.makeText(this, $e.getMessage(), Toast.LENGTH_LONG).show();

        }


    }


    private void notificar() {

        double longitud = 0.0;
        double latitud = 0.0;
        if (this.location != null) {
            longitud = this.location.getLongitude();
            latitud = this.location.getLatitude();
        } else {

            longitud = 0;
            latitud = 0;
        }

        String nombres = txt_nombres.getText().toString();
        String apellidos = txt_apellidos.getText().toString();
        Integer edad = !txt_edad.getText().toString().matches("") ? Integer.parseInt(txt_edad.getText().toString()) : 0;


        String mujer_id = "";
        if (this.mujer != null) {
            if (!rb_mujerembarazada.isChecked() && !rb_mujernoembarazada.isChecked() && !rb_noconozco.isChecked()) {

                Helpers.showTooltips(this, radio_group, "SELECCIONA UNA DE LAS  OPCIONES");
                return;
            }

            boolean validate_muerte = database.validarMujertieneNotificacionMuerte(this.mujer);
            if (validate_muerte) {
                android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog)
                        .setTitle("ERROR")
                        .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE MUERTE REGISTRADA")
                        .show();
                return;
            }
            mujer_id = this.mujer.getId();


            NotificacionMuerteMujer notificacion = new NotificacionMuerteMujer(
                    esta_embarazada,
                    Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
                    0,
                    "no enviado",
                    nombres,
                    apellidos,
                    "",
                    "",
                    edad,
                    latitud,
                    longitud,
                    mujer_id

            );
            notificacion.setEdit(0);

            Boolean connectivity = network.verificarConexion();
            if (connectivity) {
                enviarWebService(notificacion);

            } else {

                guardarBaseDatos(notificacion);
            }
        } else {
            Helpers.showTooltips(this, btn_buscar, "SELECCIONA A UNA MUJER");
//                mujer_id = "";
//                NotificacionMuerteMujer notificacion = new NotificacionMuerteMujer(
//                        esta_embarazada,
//                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
//                        0,
//                        "no enviado",
//                        nombres,
//                        apellidos,
//                        "",
//                        "",
//                        edad,
//                        latitud,
//                        longitud,
//                        mujer_id
//
//                );
//                notificacion.setEdit(0);
//
//                Boolean connectivity = network.verificarConexion();
//                if (connectivity) {
//                    enviarWebService(notificacion);
//
//                } else {
//
//                    guardarBaseDatos(notificacion);
//                }
        }


    }
}



