package evsoft.deadcode.com.sveci.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import evsoft.deadcode.com.sveci.Clases.CustomFilter;

import evsoft.deadcode.com.sveci.Clases.CustomFilter;
import evsoft.deadcode.com.sveci.R;
import evsoft.deadcode.com.sveci.model.Notificacion;

public class ListaNotificacionesAdapterRecyclerView  extends RecyclerView.Adapter<ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder> implements  View.OnClickListener {

    public ArrayList<Notificacion> notificaciones, filterList;
    private int resource;
    private Activity activity;
    CustomFilter filter;

    private View.OnClickListener listener;

    private ImageView mIconMujer;

    private AdapterView.OnItemClickListener mListener;

    public ListaNotificacionesAdapterRecyclerView(ArrayList<Notificacion> notificaciones, int resource, Activity activity) {
        this.notificaciones = notificaciones;
        this.filterList=notificaciones;
        this.resource = resource;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(resource, parent,false);

        view.setOnClickListener(this);
        return new ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder(view,mListener);


    }

    public void removeItem(int position){
         notificaciones.remove(position);
        notifyDataSetChanged();

    }

    @Override
    public void onBindViewHolder(@NonNull ListaNotificacionesAdapterRecyclerView.NotificacionesViewHolder holder, int position) {
        Notificacion noti=notificaciones.get(position);
        String _noti=noti.getNotificacion();
        String _estado=noti.getNotificacion()==""?"REGISTRADO":noti.getNotificacion();
        holder.nombres.setText(noti.getNombres());
        holder.apellidos.setText(noti.getApellidos());
        holder.direccion.setText(noti.getDireccion());
        holder.carnet.setText(noti.getCarnet());
        if(noti.getSync()==0 || noti.getSync()==3)holder.estado.setTextColor(Color.RED);
        else holder.estado.setTextColor(Color.GREEN);
        try{

        holder.estado.setText(noti.getEstado().toUpperCase());
        }
        catch (Exception $e){
            holder.estado.setText("error");
        }
//        holder.estado.setText(String.valueOf(noti.getSync()));
        holder.error.setText(noti.getError());
        holder.notificacion.setText(_estado.toUpperCase());


        holder.fecha.setText(noti.getFecha());
        switch(_noti){

                case "NME": holder.notificacion.setBackgroundResource(R.color.rosado);   break;
                case "NMM": holder.notificacion.setBackgroundResource(R.color.negro);  break;
                case "NMB": holder.notificacion.setBackgroundResource(R.color.purple); break;
                case "NP" : holder.notificacion.setBackgroundResource(R.color.orange); break;

        }
//        String edad=String.valueOf(mujer.Edad())+" AÑOS";


    }


    @Override
    public int getItemCount() {
        return notificaciones.size();
    }

    public void setOnClickListener(View.OnClickListener listener){

        this.listener=listener;
    }

    @Override
    public void onClick(View view) {

        if(this.listener!=null){
            listener.onClick(view);
        }



    }

    public class NotificacionesViewHolder extends RecyclerView.ViewHolder{


        private TextView notificacion;
        private TextView nombres;
        private TextView apellidos;
        private TextView carnet;
        private TextView direccion;
        private TextView fecha;
        private TextView estado;
        private TextView error;
        private Button btn_borrar;
        private Button bt_enviar;
        private LinearLayout layout;
        public RelativeLayout layoutABorrar;




        public NotificacionesViewHolder(View itemView, final AdapterView.OnItemClickListener listener ) {
            super(itemView);

            notificacion         =(TextView) itemView.findViewById(R.id.notificacionNotiItem);
            nombres =(TextView) itemView.findViewById(R.id.nombresNotiItem);
            apellidos =(TextView) itemView.findViewById(R.id.apellidosNotiItem);
            carnet       =(TextView) itemView.findViewById(R.id.carnetNotiItem);
            direccion        =(TextView) itemView.findViewById(R.id.direccionNotiItem);
            fecha            =(TextView) itemView.findViewById(R.id.fechaNotiItem);
            direccion       =(TextView) itemView.findViewById(R.id.direccionNotiItem);
            estado       =(TextView) itemView.findViewById(R.id.estadoNotiItem);
            error       =(TextView) itemView.findViewById(R.id.errorNotiItem);
            layout       =(LinearLayout) itemView.findViewById(R.id.layoutNoti);
            layoutABorrar=(RelativeLayout)itemView.findViewById(R.id.layoutABorrarNoti);

//            mIconMujer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//               int position=getAdapterPosition();
//
//                }
//            });



        }
    }
}
