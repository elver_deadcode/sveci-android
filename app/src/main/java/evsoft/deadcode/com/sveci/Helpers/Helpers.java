package evsoft.deadcode.com.sveci.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import evsoft.deadcode.com.sveci.R;
import it.sephiroth.android.library.tooltip.Tooltip;

public  class Helpers  {


    public static String fechaActualString(String formato){
        Date d = new Date();
        CharSequence fecha = android.text.format.DateFormat.format(formato, d.getTime());
       return  fecha.toString();

    }
    public static String getShared(String key,Context context){
        SharedPreferences sharedPreferences =  context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
          return  sharedPreferences.getString(key,"");
    }
    public static Date  fechaActual(){
        Calendar cal = Calendar.getInstance();
        Date fechaActual = cal.getTime();
        return fechaActual;

    }
    public static int numeroDiasEntreDosFechas(Date fecha1, Date fecha2){
        long startTime = fecha1.getTime();
        long endTime = fecha2.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);
        return (int)diffDays;
    }

    public  static Date  getFecha(String f1,String formato){

        Date fecha_formateada=null;
        DateFormat dateFormat  = new SimpleDateFormat(formato, Locale.getDefault());
        try {
             fecha_formateada = dateFormat.parse(f1);

        }
        catch (ParseException e){
            Log.d("fecha", "error:"+e.getMessage());
            e.printStackTrace();
        }
        return fecha_formateada;

    }

        public static  boolean compararMismoAnio(Date fecha_ini,Date fecha_fin){

        boolean result=true;

        if(fecha_ini.getYear()<fecha_fin.getYear()){
            result=false;
        }

        return result;
    }

    public  static int getEdad(String f1) {
        int age=0;

        try {
            DateFormat dateFormat = dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date fecha = dateFormat.parse(f1);
            Calendar cal = Calendar.getInstance();

            Date fechaActual = cal.getTime();
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int dIni = Integer.parseInt(formatter.format(fecha));
        int dEnd = Integer.parseInt(formatter.format(fechaActual));
         age = (dEnd-dIni)/10000;

        }
        catch (ParseException e) {
            Log.d("fecha", "error:"+e.getMessage());
            e.printStackTrace();
        }
        return age;
    }

    public static boolean compararEdad(String fecha_nacimiento,int edad, boolean menor){
        boolean result=false;
        try {


            int _edad=Helpers.getEdad(fecha_nacimiento);
//            Log.d("fecha",String.valueOf(_edad));

            if(menor){
                if(edad > _edad)
                    result=true;
                else
                    result=false;
            }
            else{
                if(edad < _edad)
                    result=true;
                else
                    result=false;
            }

        }
        catch (Exception e) {
            Log.d("fecha", "error:"+e.getMessage());
        }
        return  result;

    }

    public  static void showTooltips(Context context, View view, String message) {
        Tooltip.make(context, new Tooltip.Builder(101)
                .anchor(view, Tooltip.Gravity.LEFT)
                .closePolicy(new Tooltip.ClosePolicy()
                        .insidePolicy(true, false)
                        .outsidePolicy(true, false), 10000)
                .activateDelay(800)
                .showDelay(300)
                .text(message)
                .maxWidth(500)
                .withArrow(true)
                .withOverlay(true)
                .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                .withStyleId(R.style.MyTooltip)
                .build()
        ).show();
    }




}
