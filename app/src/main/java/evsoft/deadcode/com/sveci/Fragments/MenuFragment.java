package evsoft.deadcode.com.sveci.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import evsoft.deadcode.com.sveci.ListaMujeresActivity;
import evsoft.deadcode.com.sveci.MainActivity;
import evsoft.deadcode.com.sveci.NotificarMuerteBebeActivity;
import evsoft.deadcode.com.sveci.NotificarMuerteMujerActivity;
import evsoft.deadcode.com.sveci.NotificarMujerEmbarazadaActivity;
import evsoft.deadcode.com.sveci.NotificarPartoActivity;
import evsoft.deadcode.com.sveci.R;
import evsoft.deadcode.com.sveci.RegistroMujeresActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

        Toolbar toolbar;


    public void showToolbar( View view,String titulo, boolean upButton){

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(titulo);



    }


    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

     toolbar=(Toolbar) view.findViewById(R.id.toolbar);
     showToolbar(view,"SVECI - MENU PRINCIPAL ",false);




       CardView cv_mujerEmbarazada = (CardView) view.findViewById(R.id.card_mujerembarazada);
       CardView cv_muertemujer = (CardView) view.findViewById(R.id.card_muertemujer);
       CardView cv_muertebebe = (CardView) view.findViewById(R.id.card_muertebebe);
       CardView cv_parto = (CardView) view.findViewById(R.id.card_parto);
       CardView cv_registromujer = (CardView) view.findViewById(R.id.card_registromujer);

          cv_mujerEmbarazada.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Intent intent_notificarMujerEmbarazada = new Intent(view.getContext(),NotificarMujerEmbarazadaActivity.class);
                  startActivity(intent_notificarMujerEmbarazada);
              }
          });

        cv_muertemujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarmuertemujer = new Intent(view.getContext(),NotificarMuerteMujerActivity.class);
                startActivity(intent_notificarmuertemujer);
            }
        });

        cv_muertebebe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarmuertebebe = new Intent(view.getContext(),NotificarMuerteBebeActivity.class);
                startActivity(intent_notificarmuertebebe);
            }
        });

        cv_parto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_notificarparto = new Intent(view.getContext(),NotificarPartoActivity.class);
                startActivity(intent_notificarparto);
            }
        });

        cv_registromujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_registromujer = new Intent(view.getContext(),ListaMujeresActivity.class);
                intent_registromujer.putExtra("retroceder",false);
                startActivity(intent_registromujer);
            }
        });


       return view;




    }

}
