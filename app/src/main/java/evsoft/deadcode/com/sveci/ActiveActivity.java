package evsoft.deadcode.com.sveci;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;


import dmax.dialog.SpotsDialog;

import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Networks;

public class ActiveActivity extends AppCompatActivity {

    private Button btn_activar,btn_activarDespues;
    private EditText codigo;

    private DataConnection dataConnection;

    private  Networks network;
    Toolbar toolbar;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.menuConfiguracion:

                Intent intent_configuraciones = new Intent(this,configuracionActivity.class);
                startActivity(intent_configuraciones);
                break;

            case R.id.menuSalir:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active);

        this.btn_activar=(Button) findViewById(R.id.btn_activarTelefono);
        this.btn_activarDespues=(Button) findViewById(R.id.btn_activarMasTarde);
        this.codigo=(EditText) findViewById(R.id.txt_codigoActivacion);
        this.network=new Networks(this);


        initListeners();
        showToolbar("SVEC ",false);

    }
    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void  showDialogActivate(){
        AlertDialog dialog= new AlertDialog.Builder(ActiveActivity.this,android.R.style.Theme_Holo_Dialog)
                .setTitle("Felicitaciones!!!")
                .setMessage("El telefono se activo correctamente. Ahora es un Telefono que esta oficialmente registrado como un vigilante comunitario ")
                .setPositiveButton("Empezar a usar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent=new Intent(ActiveActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();

//                                Log.d("MainActivity", "Sending atomic bombs to Jupiter");
                    }
                })
//                .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                                Log.d("MainActivity", "Aborting mission...");
//                    }
//                })
                .show();
    }


    private void initListeners(){
        this.btn_activar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean connectivity=network.verificarConexion();

                if(connectivity){
                    enviarCodigo(codigo.getText().toString().toLowerCase());
                }
                else{

                    AlertDialog dialog= new AlertDialog.Builder(ActiveActivity.this,android.R.style.Theme_Holo_Dialog)
                            .setTitle("Sin Conexion a Internet")
                            .setMessage("El telefono no se encuentra conectado a internet. Debe conectarse para poder activar el telefono ")
                            .setPositiveButton("Entiendo", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

//                                    Intent intent=new Intent(ActiveActivity.this,MainActivity.class);
//                                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
//                                    startActivity(intent);

                                    dialog.dismiss();

                                }
                            })

                            .show();
                }




            }
        });

        this.btn_activarDespues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }

    private void enviarCodigo(String codigo_usuario){

        dataConnection=new DataConnection(this);
        final AlertDialog dialog= new SpotsDialog(ActiveActivity.this);
        dialog.show();
        dialog.setMessage("ACTIVANDO");

        dataConnection.EnviarCodigodeActivacion(new DataConnection.VolleyCallbackActivar() {
            @Override
            public void onSuccessNotificacion(JSONObject result, Boolean status) {
                try{
                    if(status && result!=null){
                        Log.i("api", "error desde el servidor ");
                        String codigo_api=result.getString("codigo");


                        if (!codigo_api.isEmpty()) {
                            SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("codigo", codigo_api);
                            editor.commit();

                             if (!sharedPreferences.getString("codigo","").isEmpty()){
                                 Log.i("shared", "codigo: "+sharedPreferences.getString("codigo",""));
                                 showDialogActivate();
                             }
                        }
                    }
                    else if(!status && result!=null){
                        Log.i("api", "estoy aca status false");
                        String mensaje=result.getString("mensaje");
                        Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG).show();

                    }
                }
                catch (Exception $e){
                    Log.i("api", $e.getMessage());
                }
                dialog.dismiss();
            }

            @Override
            public void ErrorHandler(String error) {
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        },codigo_usuario);


    }
}
