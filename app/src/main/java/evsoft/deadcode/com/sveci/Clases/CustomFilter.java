package evsoft.deadcode.com.sveci.Clases;

import android.widget.Filter;
import java.util.ArrayList;

import evsoft.deadcode.com.sveci.adapter.ListaMujeresAdapterRecyclerView;
import evsoft.deadcode.com.sveci.model.Mujer;

public class CustomFilter  extends  Filter{

    ListaMujeresAdapterRecyclerView adapter;
    ArrayList<Mujer> filterList;

    public CustomFilter(ArrayList<Mujer> filterList, ListaMujeresAdapterRecyclerView adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();

        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Mujer> filteredMujeres=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getNombres().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredMujeres.add(filterList.get(i));
                }
                if(filterList.get(i).getApellidopaterno().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredMujeres.add(filterList.get(i));
                }
            }

            results.count=filteredMujeres.size();
            results.values=filteredMujeres;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.mujeres= (ArrayList<Mujer>) results.values;

        //REFRESH
        adapter.notifyDataSetChanged();
    }
}
