package evsoft.deadcode.com.sveci.model;

import android.content.ContentValues;

import java.util.Date;

import evsoft.deadcode.com.sveci.Helpers.Helpers;

public class Notificacion {
    private static final int NUM_ROWS = 13;
    private String id;
    private int sync;
    private String fecha;
    private String estado;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String telefono;
    private String carnet;
    private int edit;
    private String error;
    private String notificacion;
    public Notificacion(){}

    public Notificacion(
            String id,
            int sync,
            String fecha,
            String estado,
            String nombres,
            String apellidos,
            String direccion,
            String telefono,
            String carnet,
            int edit,
            String error,
            String notificacion

    ) {
        this.id=id;
        this.sync=sync;
        this.fecha=fecha;
        this.estado=estado;
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.direccion=direccion;
        this.telefono=telefono;
        this.carnet=carnet;

        this.edit=edit;
        this.error=error;
        this.notificacion=notificacion;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public String getSyncString(){
        if(this.sync==1){
            return "ENVIADO";
        }
        return "NO ENVIADO";
    }
    public Date getFechaDate(){
        Date _date=null;
        _date=Helpers.getFecha(this.fecha,"y-m-d H:m:s");
        return _date;
    }
    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }



    public int getEdit() {
        return edit;
    }

    public void setEdit(int edit) {
        this.edit = edit;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getNotificacion() {
        return notificacion;
    }
    public String tablaNotificacion(){
        String _notificacion="";
        switch(this.notificacion){
            case "NME": _notificacion="notificaciones_embarazo"; break;
            case "NMM": _notificacion="notificaciones_muerte_mujer"; break;
            case "NMB": _notificacion="notificaciones_muerte_bebe"; break;
            case "NP":  _notificacion="notificaciones_parto"; break;
        }
        return _notificacion;
    }

    public void setNotificacion(String notificacion) {
        this.notificacion = notificacion;
    }

    public ContentValues toValues(){
        ContentValues contentValues=new ContentValues(NUM_ROWS);
        contentValues.put("id",id);
        contentValues.put("sync",sync);
        contentValues.put("fecha",fecha);
        contentValues.put("estado",estado);
        contentValues.put("nombres",nombres);
        contentValues.put("apellidos",apellidos);
        contentValues.put("direccion",direccion);
        contentValues.put("telefono",telefono);
        contentValues.put("carnet",carnet);
        contentValues.put("edit",edit);
        contentValues.put("error",error);
        contentValues.put("notificacion",notificacion);
        return contentValues;
    }
}
