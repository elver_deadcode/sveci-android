package evsoft.deadcode.com.sveci;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.DateInputMask;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Clases.ValidateHelper;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import fr.ganfra.materialspinner.MaterialSpinner;

public class RegistroMujeresActivity extends AppCompatActivity {


    private ValidateHelper validator;

    private boolean status_api_sus=false;

    //inputsLayouts
    private TextInputLayout  ly_regNombres;
    private TextInputLayout  ly_regApellidoPaterno;
    private TextInputLayout  ly_regApellidoMaterno;
    private TextInputLayout  ly_regCarnet;
    private TextInputLayout  ly_regFechaNacimiento;
    private TextInputLayout  ly_regDireccion;
    private TextInputLayout  ly_regTelefono;
    private TextInputLayout  ly_complemento;

    //inputs
    private EditText nombres;
    private EditText apellidoPaterno;
    private EditText apellidoMaterno;
    private EditText numeroCarnet;
    private EditText fechaNacimiento;
    private EditText direccion;
    private EditText telefono;
    private EditText complemento;

    //input dialog buscar persona
    private TextInputLayout ly_numcarnet_buscar;
    private TextInputLayout  ly_fechanacimiento_buscar;
    private TextInputLayout  ly_complemento_buscar;

    private EditText numcarnet_buscar;
    private EditText fechanacimiento_buscar;
    private EditText complemento_buscar;
    private AlertDialog dialog_buscarpersona;
    private Button btn_buscarpersona;
    private Button btn_regresar;
    //botones
    private Button btn_cancelar;
    private Button btn_guardar;
    private Calendar calendar;
    private int year,month,day;
    private Mujer mujer;
    private Database database;
    private Networks network;
    private DataConnection webservice;
    private boolean retroceder;
    private boolean editar=false;
    private boolean adscrito=false;
    private int adscrito_id=0;
    private Context _context;

    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_mujeres);


        initView();
        initListeners();


        if(network.verificarConexion()){
            Log.i("CONEXION","SI HAY CONEXION A INTERNETE");
                showDialogBuscarPersona();
            //this.ApiStatus();

        }
        else{
            Log.i("CONEXION","NO HAY CONEXION AN INTERNET");
            limpiarFormularioRegistrar();
        }

        calendar=Calendar.getInstance();
        year=calendar.get(Calendar.YEAR);
        month=calendar.get(Calendar.MONTH);
        day=calendar.get(Calendar.DAY_OF_MONTH);
//        showDate(year,month+1,day);


    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year    , month, day);
        }
        return null;
    }

    private void showDialogBuscarPersona(){
        AlertDialog.Builder mBuilder=new AlertDialog.Builder(RegistroMujeresActivity.this);
        View nview= getLayoutInflater().inflate(R.layout.dialog_buscar_persona,null);
        mBuilder.setView(nview);
        dialog_buscarpersona=mBuilder.create();
        dialog_buscarpersona.setCancelable(false);
        dialog_buscarpersona.setCanceledOnTouchOutside(false);

        dialog_buscarpersona.show();


        this.numcarnet_buscar=nview.findViewById(R.id.txt_numcarnet_buscar);
        this.fechanacimiento_buscar=nview.findViewById(R.id.txt_fechanacimiento_buscar);
        this.complemento_buscar=nview.findViewById(R.id.txt_complemento_buscar);
        this.btn_buscarpersona=nview.findViewById(R.id.btn_buscarPersona);
        this.btn_regresar=nview.findViewById(R.id.btn_regresar);
        this.ly_numcarnet_buscar=nview.findViewById(R.id.ly_numcarnet_buscar);
        this.ly_fechanacimiento_buscar=nview.findViewById(R.id.ly_fechanacimiento_buscar);
        this.ly_complemento_buscar=nview.findViewById(R.id.ly_complemento_buscar);


        new DateInputMask(fechanacimiento_buscar);
        this.btn_buscarpersona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( checkValidationBuscarPersona()){
                     String numcarnet=numcarnet_buscar.getText().toString().trim();
                     String fechanac=fechanacimiento_buscar.getText().toString().trim();
                     String complemento=complemento_buscar.getText().toString().trim();

                     buscarMujerApiWebService(numcarnet,fechanac,complemento);
                }

            }
        });
        this.btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finalizar();

            }
        });

    }
    private void llenarFormularioBasico(String numcarnet,String complemento,String fechanacimiento){
        this.numeroCarnet.setText(numcarnet);
        this.fechaNacimiento.setText(fechanacimiento);
        this.complemento.setText(complemento);
        adscrito=false;
        adscrito_id=0;
    }

    private void llenarFormulario(JSONObject datospersona){

        String _nombres="";
        try{_nombres=datospersona.getString("nombres");} catch (JSONException e){};
        String _primerApellido="";
        try{
            if(datospersona.has("primerapellido"))
            _primerApellido=datospersona.getString("primerapellido");
            else if(datospersona.has("primerApellido"))
                _primerApellido=datospersona.getString("primerApellido");

        } catch (JSONException e){};
        String _segundoApellido="";
        try{
            if(datospersona.has("segundoapellido"))
            _segundoApellido=datospersona.getString("segundoapellido");
            else if(datospersona.has("segundoApellido"))
                _segundoApellido=datospersona.getString("segundoApellido");

        } catch (JSONException e){};
        String _numcarnet="";
        try{
            if(datospersona.has("ci"))
            _numcarnet=datospersona.getString("ci");
            else if(datospersona.has("numeroCarnet"))
                _numcarnet=datospersona.getString("numeroCarnet");

        } catch (JSONException e){};
        String _fechanacimiento="";
        try{
            if(datospersona.has("fechanacimiento"))
            _fechanacimiento=datospersona.getString("fechanacimiento");
            else if(datospersona.has("fechaNacimiento"))
            _fechanacimiento=datospersona.getString("fechaNacimiento");


        } catch (JSONException e){};
        String _complemento="";
        try{_complemento=datospersona.getString("complemento");} catch (JSONException e){};
        String _telefono="";
        try{
            if(datospersona.has("tel_referencia"))
            _telefono=datospersona.getString("tel_referencia");
            else   if(datospersona.has("celular"))
                _telefono=datospersona.getString("celular");

        } catch (JSONException e){};
        String _direccion="";
        try{_direccion=datospersona.getString("direccion");} catch (JSONException e){};
        int _adscrito_id=0;
        try{_adscrito_id=datospersona.getInt("adscrito_id");} catch (JSONException e){};

            this.nombres.setText(_nombres);
            this.apellidoPaterno.setText(_primerApellido);
            this.apellidoMaterno.setText(_segundoApellido);
            this.numeroCarnet.setText(_numcarnet);
            this.fechaNacimiento.setText(_fechanacimiento);
            this.direccion.setText(_direccion);
            this.telefono.setText(_telefono);
            this.complemento.setText(_complemento);
            adscrito=true;

    }


    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        fechaNacimiento.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    private Boolean checkValidation(){

        if (!validator.isEditTextFilled(nombres, ly_regNombres, "Error: Tienes que escribir un nombre.")) {
            return false;
        }
        if (!validator.isEditTextFilled(apellidoPaterno, ly_regApellidoPaterno, "Error: Tienes que escribir tu primer apellido.")) {
            return false;
        }

        if (!validator.isEditTextFilled(numeroCarnet, ly_regCarnet, "Error: Tienes que escribir un numero de carnet.")) {
            return false;
        }

        if (!validator.isEditTextFilled(fechaNacimiento, ly_regFechaNacimiento, "Error: Tienes elegir una fecha de nacimiento.")) {
            return false;
        }


        if (!validator.isFechaNacimientoValida(fechaNacimiento, ly_regFechaNacimiento, "Error: La fecha de nacimiento debe  ser mayor a 10 años.",10,59)) {
            return false;
        }
        return true;



    }

    private Boolean checkValidationBuscarPersona(){

        if (!validator.isEditTextFilled(numcarnet_buscar, ly_numcarnet_buscar, "Error: Tienes que escribir numero de carnet.")) {
            return false;
        }
        if (!validator.isEditTextFilled(fechanacimiento_buscar, ly_fechanacimiento_buscar, "Error: Tienes que escribir fecha de nacimiento.")) {
            return false;
        }
        if (!validator.isFechaNacimientoValida(fechanacimiento_buscar, ly_fechanacimiento_buscar, "Error: La fecha de nacimiento debe  ser mayor a 10 años.",10,59)) {
            return false;
        }
        return true;
    }
    private void limpiarFormularioRegistrar(){
        this.adscrito_id=0;
        this.nombres.setEnabled(true);
        this.apellidoPaterno.setEnabled(true);
        this.apellidoMaterno.setEnabled(true);
        this.numeroCarnet.setEnabled(true);
        this.fechaNacimiento.setEnabled(true);
        this.direccion.setEnabled(true);
        this.complemento.setEnabled(true);

    }


    private void initView() {

        this.ly_regNombres=findViewById(R.id.ly_regNombres);
        this.ly_regApellidoPaterno=findViewById(R.id.ly_regApellidoPaterno);
        this.ly_regApellidoMaterno=findViewById(R.id.ly_regApellidoMaterno);
        this.ly_regCarnet=findViewById(R.id.ly_regCarnet);
        this.ly_regFechaNacimiento=findViewById(R.id.ly_regFechaNacimiento);
        this.ly_regDireccion=findViewById(R.id.ly_regDireccion);
        this.ly_regTelefono=findViewById(R.id.ly_regTelefono);
        this.ly_complemento=findViewById(R.id.ly_complemento);

        this.nombres=findViewById(R.id.txt_regNombres);
        this.apellidoPaterno= findViewById(R.id.txt_regApellidoPaterno);
        this.apellidoMaterno= findViewById(R.id.txt_regApellidoMaterno);
        this.numeroCarnet= findViewById(R.id.txt_regCarnet);
        this.fechaNacimiento= findViewById(R.id.txt_regFechaNacimiento);
        new DateInputMask(fechaNacimiento);
//        fechaNacimiento.setFocusable(false);
        this.direccion= findViewById(R.id.txt_regDireccion);
        this.telefono= findViewById(R.id.txt_regTelefono);
        this.complemento= findViewById(R.id.txt_complemento);

        this.btn_cancelar=findViewById(R.id.btn_cancelarRegMujer);
        this.btn_guardar=findViewById(R.id.btn_guardarRegMujer);

//        new DateInputMask(this.nombres);


        validator = new ValidateHelper(this);

        database= new Database(this);

        this.network=new Networks(this);

        Intent i =getIntent();

        this.retroceder=i.getBooleanExtra("retroceder",false);

        Intent i2 =getIntent();
        this.editar=i2.getBooleanExtra("editar",false);


        if(editar)
        {
            this.mujer=(Mujer) i2.getSerializableExtra("mujer");
            cargarValores(mujer);
        }



    }
    private void cargarValores(Mujer mujer){

        this.nombres.setText(mujer.getNombres().toString());
        this.apellidoPaterno.setText(mujer.getApellidopaterno().toString());
        this.apellidoMaterno.setText(mujer.getApellidomaterno().toString());
        this.numeroCarnet.setText(mujer.getCarnet().toString());
        this.fechaNacimiento.setText(mujer.getFechanacimiento().toString());
        this.direccion.setText(mujer.getDireccion().toString());
        this.telefono.setText(mujer.getTelefono().toString());
    }

    private void finalizar(){
        Intent intent =new Intent();
        Bundle bundle=new Bundle();
        bundle.putSerializable("mujer",mujer);
        intent.putExtras(bundle);

        setResult(RESULT_OK,intent);
        finish();
    }


    private void initListeners() {

        this.btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalizar();
            }
        });



        this.btn_guardar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if( checkValidation()){

                    if(editar==false) {

                        mujer = new Mujer();
                        mujer.setId(mujer.generateUUID());
                        mujer.setEdit(0);

                    }
                    else{
                        mujer.setEdit(1);
                    }

                        mujer.setNombres(nombres.getText().toString().trim().toUpperCase());
                        mujer.setApellidopaterno(apellidoPaterno.getText().toString().trim().toUpperCase());
                        mujer.setApellidomaterno(apellidoMaterno.getText().toString().trim().toUpperCase());
                        mujer.setFechanacimiento(fechaNacimiento.getText().toString().trim());
                        mujer.setCarnet(numeroCarnet.getText().toString().trim());
                        mujer.setDireccion(direccion.getText().toString().trim());
                        mujer.setTelefono(telefono.getText().toString().trim());
                        mujer.setSync(0);
                        mujer.setAdscrito(adscrito_id);
                        mujer.setComplemento(complemento.getText().toString().trim());
                        guardarForm(mujer);
                }


            }
        });



    }

    public void guardarForm(Mujer mujer){

        Boolean connectivity=network.verificarConexion();


        if(connectivity){

            enviarWebService(mujer);
        }
        else{

            Toast.makeText(this,"No existe conexion a internet.",Toast.LENGTH_SHORT).show();

            guardarBaseDatos(mujer);
        }

    }

    private void buscarMujerApiWebService(final String numcarnet, String fechanacimiento, String complemento){

        webservice=new DataConnection(this);
        final AlertDialog loading_buscar_persona= new SpotsDialog(RegistroMujeresActivity.this);
        loading_buscar_persona.show();
        loading_buscar_persona.setMessage("BUSCANDO");
        webservice.BuscarMujerApi(new DataConnection.VolleyCallbackBuscarMujerApi() {
            @Override
            public void onSuccess(JSONObject result, Boolean res) {
                try{
                    if(res) {
                        JSONObject data = result.getJSONObject("data");
                        String origen=data.getString("origen");
                        if(origen.equals("svec")) {
                            String mensaje = "Esta persona ya esta registrada en el SVEC";
                            Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                        }
                        else{
                            String sexo = data.getString("sexo");
                            if (sexo.equals("Femenino") || sexo.equals("F")) {
                                llenarFormulario(data);
                                dialog_buscarpersona.dismiss();

                            } else {
                                Toast.makeText(getApplicationContext(), "ESTOS DATOS NO PERTENECEN A UNA MUJER", Toast.LENGTH_LONG).show();

                            }
                        }

                    }
                    else{
                        String mensaje = "no se pudo conectar con el servidor";
                        Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e){
                    AlertDialog dialog = new android.app.AlertDialog.Builder(RegistroMujeresActivity.this, android.R.style.Theme_Holo_Dialog)
                            .setTitle("NO SE ENCONTRO A LA MUJER")
                            .setMessage("POR FAVOR COMPLETE LOS NOMBRES Y APELLIDOS DE LA MUJER")
                            .setPositiveButton("Volver a buscar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("COMPLETAR NOMBRES ", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    adscrito=false;
                                    adscrito_id=0;
                                    llenarFormularioBasico(numcarnet_buscar.getText().toString(),complemento_buscar.getText().toString(),fechanacimiento_buscar.getText().toString());
                                    limpiarFormularioRegistrar();
                                    dialog.dismiss();
                                    dialog_buscarpersona.dismiss();
                                }
                            }).show();

                    Log.i("NO SE ENCONTRO",e.getMessage());
                }
               loading_buscar_persona.dismiss();
            }

            @Override
            public void ErrorHandler(String error) {
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                loading_buscar_persona.dismiss();
            }

        }, numcarnet,fechanacimiento,complemento);

    }

    private void ApiStatus(){

        webservice=new DataConnection(this);
        webservice.ApiStatus(new DataConnection.VolleyCallbackApiStatus() {
            @Override
            public void onSuccess(JSONObject result, Boolean res) {

                if(res){
                    try {
                        String stat = result.getString("status");
                        if(stat=="ok"){
                            status_api_sus=true;

                        }
                    }
                    catch (Exception e){
                        Toast.makeText(RegistroMujeresActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                        Log.i("error",e.getMessage()+" result::"+result.toString());
                    }


                }


            }

            @Override
            public void ErrorHandler(String error) {

            }
        });



    }


    private void enviarWebService(final Mujer mujer){

        Log.i("response","private void enviarWebService(final Mujer mujer):"+mujer.toValues());

        webservice=new DataConnection(this);
        final  String cod_telefono= Helpers.getShared("codigo",this);
        final AlertDialog dialog=new SpotsDialog(this);
        dialog.show();
        dialog.setMessage("Guardando Datos...");

        webservice.EnviarMujer(new DataConnection.VolleyCallbackMujer() {
            @Override
            public void onSuccess(JSONObject result, Boolean res) {
                try{
                if(res) {


                        JSONObject data = result.getJSONObject("data");
                        String mensaje = result.getString("mensaje");
                        mujer.setSync(1);
                        mujer.setEdit(0);
                        guardarBaseDatos(mujer);

                }
                        else{
                            String mensaje = result.getString("mensaje");
                            Toast.makeText(getApplicationContext(),"WEBSERVICE:"+mensaje, Toast.LENGTH_LONG).show();
                        }
                }
                catch (Exception e){

                    Toast.makeText(RegistroMujeresActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                        Log.i("error",e.getMessage()+" result::"+result.toString());
                }


                dialog.dismiss();
            }

            @Override
            public void ErrorHandler(String error) {

                Toast.makeText(RegistroMujeresActivity.this,error,Toast.LENGTH_LONG).show();
                mujer.setSync(0);

                guardarBaseDatos(mujer);


                dialog.dismiss();
            }

        }, mujer, cod_telefono);


    }

    private void guardarBaseDatos(Mujer mujer){


        try{

            if(this.editar==false && mujer.getEdit()==0) {

                database.insertMujer(mujer);
            }
            else {
                database.updateMujer(mujer);
            }


            String msg="Los datos de la mujer se enviaron  su centro de salud correctamente. Gracias!!!";

            if(mujer.getSync()==0){
                msg="Los datos de la mujer  no se pudieron enviar al centro de salud. Sin embargo esta se guardo en su celular y se  enviara una vez se conecte a internet. Gracias!!!";
            }

            if(this.editar==false) {


                android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog)
                        .setTitle("REGISTRO EXITOSO")
                        .setMessage(msg)
                        .setPositiveButton("Regresar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finalizar();
                            }
                        })
                        .setNegativeButton("Registrar otra mujer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                limpiarFormulario();

                            }
                        }).show();
            }
            else{

                android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog)
                        .setTitle("REGISTRO EXITOSO")
                        .setMessage(msg)
                        .setPositiveButton("Regresar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finalizar();
                            }
                        })
                        .show();
            }

        }
        catch (Exception $e){

        }

    }

    public void limpiarFormulario(){

        mujer=null;
        nombres.setText("");
        apellidoMaterno.setText("");
        apellidoPaterno.setText("");
       numeroCarnet.setText("");
       fechaNacimiento.setText("");
       direccion.setText("");
       telefono.setText("");
       nombres.requestFocus();


    }


}
