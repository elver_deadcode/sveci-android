package evsoft.deadcode.com.sveci;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Clases.SwipeHelper;
import evsoft.deadcode.com.sveci.Clases.SwipeHelperMujer;
import evsoft.deadcode.com.sveci.Clases.SyncService;
import evsoft.deadcode.com.sveci.adapter.ListaMujeresAdapterRecyclerView;
import evsoft.deadcode.com.sveci.adapter.ListaNotificacionesAdapterRecyclerView;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.Notificacion;


public class ListaMujeresActivity extends AppCompatActivity implements SwipeHelperMujer.RecyclerItemTouchHelperListener {


    public static final int REQUEST_CODE = 1;
    private FloatingActionButton fabaddMujer;
    private RecyclerView recycler;


    private SearchView sv;
    private ArrayList<Mujer> listaMujeres;
    private Database database;
    private Boolean retroceder=false;
    public  ListaMujeresAdapterRecyclerView listaMujeresAdapterRecyclerView;
    private int position;
    private Mujer mujer_aux;
    private Button btn_enviar_mujeres;
    private Networks network;
    private Intent intent_service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_mujeres);
        initActivity();
        initListeners();
        initRecycler();
        ItemTouchHelper.SimpleCallback simpleCallback= new SwipeHelperMujer(0,ItemTouchHelper.LEFT,ListaMujeresActivity.this);
        new  ItemTouchHelper(simpleCallback).attachToRecyclerView(recycler);
    }

    private void initActivity() {

        Intent i =getIntent();
        this.retroceder=i.getBooleanExtra("retroceder",false);
        database=new Database(this);
        fabaddMujer= (FloatingActionButton) findViewById(R.id.fabAddMujer);
        this.btn_enviar_mujeres= (Button) findViewById(R.id.btnEnviarListaMujeres);
        this.listaMujeresAdapterRecyclerView= new ListaMujeresAdapterRecyclerView(getMujeres(),R.layout.mujer_description_item,this);

        recycler = findViewById(R.id.listaMujeresRecycler);

        sv=(SearchView) findViewById(R.id.mSearch);
        showToolbar("MUJERES", true);

    }
    private void enviarRegistros(){
        this.network=new Networks(this);


        Boolean esta_iniciado_servicio=isMyServiceRunning(SyncService.class);

        Boolean connectivity = network.verificarConexion();

        if(!esta_iniciado_servicio && connectivity){

            iniciarServicio();
            final android.app.AlertDialog dialog=new SpotsDialog(this);
            dialog.show();
            dialog.setMessage("Enviando Registros de Mujeres...");
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                           getMujeres();

                        }
                    },
                    5000
            );
        }
        else{
            AlertDialog dialog = new AlertDialog.Builder(ListaMujeresActivity.this, android.R.style.Theme_Holo_Dialog)
                    .setTitle("CONEXION A INTERNET ")
                    .setMessage("PARA PODER ENVIAR LOS REGISTROS  NECESITAS ESTAR CONECTADO A  INTERNET")
                    .setNegativeButton("CERRAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                     dialog.dismiss();

                        }
                    }).show();
        }


    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    private void iniciarServicio(){
        intent_service=new Intent(this, SyncService.class);


        this.startService(intent_service);
    }

    private void initListeners(){
        fabaddMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(ListaMujeresActivity.this,RegistroMujeresActivity.class);
                intent.putExtra("retroceder",true);

                startActivityForResult(intent,REQUEST_CODE);


            }
        });
        btn_enviar_mujeres.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                enviarRegistros();


            }

        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void showToolbar(String titulo, boolean upButton){
       Toolbar toolbar=(Toolbar) findViewById(R.id.toolbarMujeres);
       setSupportActionBar(toolbar);
       getSupportActionBar().setTitle(titulo);
       getSupportActionBar().setSubtitle("Registradas");

       getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);

    }

    private void initRecycler() {
        listaMujeres=getMujeres();
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(linearLayoutManager);
        this.listaMujeresAdapterRecyclerView= new ListaMujeresAdapterRecyclerView(listaMujeres,R.layout.mujer_description_item,this);
        recycler.setAdapter(listaMujeresAdapterRecyclerView);

        listaMujeresAdapterRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mujer mujer=listaMujeres.get(recycler.getChildAdapterPosition(view));
                Log.i("position",String.valueOf(recycler.getId()));
                if(retroceder){

                enviarActivity2(mujer);
                }
                else {

//                    Intent intent=new Intent(getApplicationContext(), RegistroMujeresActivity.class);
//
//                    intent.putExtra("editar",true);
//                    intent.putExtra("mujer",mujer);
//                    startActivityForResult(intent,REQUEST_CODE);
                }

            }
        });

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                listaMujeresAdapterRecyclerView.getFilter().filter(query);
                return false;
            }
        });
    }


    public ArrayList<Mujer> getMujeres(){
        ArrayList<Mujer> mujeres= database.getMujeresAll();
        return mujeres;
    }




    public void enviarActivity2(Mujer mujer){
        Intent intent =new Intent();
        Bundle bundle=new Bundle();
        bundle.putSerializable("mujer",mujer);
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(REQUEST_CODE==requestCode && resultCode==RESULT_OK ){
            initRecycler();



        }
    }

    @Override
    public void finish() {

        super.finish();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, final int position) {
        try{
            if(viewHolder instanceof ListaMujeresAdapterRecyclerView.MujerViewHolder)
            {
                this.position=viewHolder.getAdapterPosition();
                this.mujer_aux=listaMujeres.get(viewHolder.getAdapterPosition());
                int sync=mujer_aux.getSync();
                if(sync==1){
                    recycler.getAdapter().notifyItemChanged(position);


                    Toast.makeText(getApplicationContext(),"NO SE PUEDE BORRAR UN REGISTRO DE UNA MUJER   QUE YA SE ENVIO", Toast.LENGTH_LONG).show();

                }
                else{
                    AlertDialog dialog = new AlertDialog.Builder(ListaMujeresActivity.this, android.R.style.Theme_Holo_Dialog)
                            .setTitle("BORRAR  REGISTRO ")
                            .setMessage("ESTA SEGURO DE BORRAR EL REGISTRO DE ESTA MUJER?")
                            .setPositiveButton("SI BORRAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    recycler.getAdapter().notifyItemChanged(position);
                                    database.borrarMujer(mujer_aux.getId());

                                    listaMujeresAdapterRecyclerView.removeItem(position);
                                    listaMujeresAdapterRecyclerView.notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    recycler.getAdapter().notifyItemChanged(position);
                                }
                            }).show();
                }
            }
        }
        catch (Exception e){

            Log.d("POSITION","ERROR::"+e.getMessage());
        }

    }
}
