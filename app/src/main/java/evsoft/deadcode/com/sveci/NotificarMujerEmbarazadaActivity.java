package evsoft.deadcode.com.sveci;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;


import dmax.dialog.SpotsDialog;
import evsoft.deadcode.com.sveci.Clases.AccessLocation;
import evsoft.deadcode.com.sveci.Clases.DBConnect;
import evsoft.deadcode.com.sveci.Clases.DataConnection;
import evsoft.deadcode.com.sveci.Clases.Database;
import evsoft.deadcode.com.sveci.Clases.GPSTracker;
import evsoft.deadcode.com.sveci.Clases.Networks;
import evsoft.deadcode.com.sveci.Helpers.Helpers;
import evsoft.deadcode.com.sveci.model.Mujer;
import evsoft.deadcode.com.sveci.model.Notificacion;
import evsoft.deadcode.com.sveci.model.NotificacionEmbarazada;
import it.sephiroth.android.library.tooltip.Tooltip;

import static java.lang.String.*;


public class NotificarMujerEmbarazadaActivity extends AppCompatActivity   {


    public static final int REQUEST_CODE = 1;
    private Button btn_notificar;
private FloatingActionButton btn_buscar;
private Toolbar toolbar;

private TextInputEditText txt_nombres;
private TextInputEditText txt_apellidos;
private TextInputEditText txt_edad;
private Mujer mujer;
private Networks network;
private Button btn_limpiar;
private View view;


private Boolean mRequestingLocationUpdates=false;
private LocationRequest mLocationRequest;
private Location mLastLocation;

private static final int  UPDATED_INTERVAL=5000;
private static final int  FASTED_INTERVAL=3000;
private static final int  DISPLACEMENT=10;

private FusedLocationProviderClient mfusedLocationProviderClient;




private Boolean isActiveGps=false;

private GPSTracker gpsTracker;
private Location location;



private DataConnection webservice;

private Database database;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificar_mujer_embarazada);


              initActivity();
              showToolbar("NOTIFICAR MUJER EMBARAZADA",true);
              initiListeners();



        gpsTracker=new GPSTracker(getApplicationContext(),NotificarMujerEmbarazadaActivity.this);

        if (!gpsTracker.checkPermissions()) {

            requestPermissions();

        } else {

            this.location=gpsTracker.getLocation();

        }


    }



    @Override
    public void onStart() {
        super.onStart();

    }
    private void requestPermissions() {
        boolean shouldProvideRationale =ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale) {

            startLocationPermissionRequest();


        } else {

            startLocationPermissionRequest();
        }
    }
    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
               1);
    }


        private void deshabilitarCampos(){
        txt_nombres.setEnabled(false);
        txt_apellidos.setEnabled(false);
        txt_edad.setEnabled(false);
        }

    private void initActivity() {

        btn_notificar=(Button) findViewById(R.id.btn_notiembarazo);
        txt_nombres=(TextInputEditText)findViewById(R.id.txt_notiEmbarazoNombres);
        txt_apellidos=(TextInputEditText)findViewById(R.id.txt_notiEmbarazoApellidos);
        txt_edad=(TextInputEditText)findViewById(R.id.txt_notiEmbarazoEdad);

        database=new Database(this);


        btn_buscar=(FloatingActionButton) findViewById(R.id.fab_buscarMujerEmbarazo);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.rosado);


        mujer=null;

        btn_limpiar=(Button) findViewById(R.id.btn_notiEmbarazoLimpiar);

        this.network=new Networks(this);
        deshabilitarCampos();

        ArrayList<Notificacion> notis= database.getNotificaciones();
        for (int i = 0 ; i < notis.size() ; i++)
            Log.d("OBJECT:::" , notis.get(i).toValues().toString());


    }
    private void initiListeners() {


        btn_notificar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
              notificar();
            }

          });

        btn_buscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                buscarMujer();
            }

        });

        btn_limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarVista();
            }
        });


    }

    private void enviarWebService(final NotificacionEmbarazada notificacion){

        webservice=new DataConnection(this);
        final  String cod_telefono=Helpers.getShared("codigo",this);

        Log.i("notificacion",notificacion.toValues().toString());


        btn_notificar.setEnabled(false);

        final android.app.AlertDialog dialog=new SpotsDialog(this);
        dialog.show();
        dialog.setMessage("Guardando Notificacion...");


        webservice.EnviarNotificacion(
                new DataConnection.VolleyCallback() {
                    @Override
                    public void onSuccessNotificacion(JSONObject response_json, Boolean status) {

                        try{
                        if(status ) {



                            JSONObject data = response_json.getJSONObject("data");
                             int id_api=data.getInt("IdNotificacionEmbarazo");
                             notificacion.setId_api(id_api);
                             notificacion.setEstado("enviado");
                             notificacion.setSync(1);

                             guardarBaseDatos(notificacion);

//                            Log.i("response","response="+data);
                            Log.i("response","notificaion="+notificacion.toValues());

                              }
                            else{
                            String msg=response_json.getString("mensaje");

                            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                                Log.i("status","error");

                            }
                        }
                        catch (Exception e){ }

                        btn_notificar.setEnabled(true);
                        dialog.dismiss();

                    }
                    @Override
                    public void ErrorHandler(String error) {
                        Log.i("debug","void ErrorHandler(String error)"+error);
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                        btn_notificar.setEnabled(true);
                        dialog.dismiss();
                    }


                },
                notificacion,cod_telefono
        );


    }

    private void guardarBaseDatos(NotificacionEmbarazada notificacion){



        try{

        database.insertNotificacionEmbarazo(notificacion);

              String msg="La notificacion de Mujer Embarazada se envio  su centro de salud correctamente. Gracias!!!";

              if(notificacion.getSync()==0){
                  msg="La notificacion de Mujer Embarazada no se pudo enviar al centro de salud. Sin embargo  se guardo en su celular y se  enviara una vez se conecte a internet. Gracias!!!";
              }


                    android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                    .setTitle("NOTIFICACION EXITOSA")
                    .setMessage(msg)
                    .setPositiveButton("VER HISTORIAL DE NOTIFICACIONES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent=new Intent(NotificarMujerEmbarazadaActivity.this,ListaNotificaciones.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .show();
                    dialog.setCanceledOnTouchOutside(false);

        }
        catch (Exception $e){
            Toast.makeText(this,$e.getMessage(),Toast.LENGTH_LONG).show();

        }


    }

    private void notificar() {

        double longitud = 0.0;
        double latitud = 0.0;

        if (this.location!=null) {
//            this.location = l;
            longitud = location.getLongitude();
            latitud = location.getLatitude();
        }
        else{
            longitud = 0;
            latitud = 0;
        }


            String nombres = txt_nombres.getText().toString();
            String apellidos = txt_apellidos.getText().toString();
            Integer edad = !txt_edad.getText().toString().matches("") ? Integer.parseInt(txt_edad.getText().toString()) : 0;

            String mujer_id = "";

            if (this.mujer != null) {
                    boolean validate=database.validarCantidadDias(this.mujer,"notificaciones_embarazo",31);
                    boolean validate_muerte=database.validarMujertieneNotificacionMuerte(this.mujer);

                            if(validate_muerte){
                                android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                                        .setTitle("ERROR")
                                        .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE MUERTE REGISTRADA")
                                        .show();
                                 return;
                            }
                             if(!validate){
                                 android.app.AlertDialog dialog= new android.app.AlertDialog.Builder(this,android.R.style.Theme_Holo_Dialog)
                                         .setTitle("ERROR")
                                         .setMessage("ESTA MUJER YA TIENE UNA NOTIFICACION DE EMBARAZO RECIENTEMENTE")
                                         .show();
                                return;
                            }
                            else
                                {

                                    mujer_id = this.mujer.getId();
                                    NotificacionEmbarazada notificacion = new NotificacionEmbarazada(
                                            "embarazo",
                                            Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
                                            0,
                                            "no enviado",
                                            nombres,
                                            apellidos,
                                            "",
                                            "",
                                            edad,
                                            latitud,
                                            longitud,
                                            mujer_id

                                    );
                                    notificacion.setEdit(0);

                                    Boolean connectivity = network.verificarConexion();
                                    if (connectivity) {
                                       enviarWebService(notificacion);
                                    } else {

                                       guardarBaseDatos(notificacion);
                                    }

                            }



            }
            else {
                            Helpers.showTooltips(this,btn_buscar,"SELECCIONA A UNA MUJER");


//                mujer_id = "";
//                NotificacionEmbarazada notificacion = new NotificacionEmbarazada(
//                        "embarazo",
//                        Helpers.fechaActualString("yyyy-MM-dd HH:mm:ss"),
//                        0,
//                        "no enviado",
//                        nombres,
//                        apellidos,
//                        "",
//                        "",
//                        edad,
//                        latitud,
//                        longitud,
//                        mujer_id
//
//                );
//                notificacion.setEdit(0);
//
//                Boolean connectivity = network.verificarConexion();
//                if (connectivity) {
//                    enviarWebService(notificacion);
//                } else {
//
//                    guardarBaseDatos(notificacion);
//                }


            }


    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(REQUEST_CODE==requestCode && resultCode==RESULT_OK ){
            if( data.getExtras()!=null){

                mujer=(Mujer) data.getExtras().getSerializable("mujer");
                btn_limpiar.setVisibility(View.VISIBLE);
                llenarformulario2(mujer);

            }

        }
    }



    private void buscarMujer(){

        Intent intent=new Intent(this, ListaMujeresActivity.class);
        intent.putExtra("retroceder",true);

        startActivityForResult(intent,REQUEST_CODE);

    }

    public void showToolbar(String titulo, boolean upButton){
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);

       getSupportActionBar().setTitle(titulo);
       getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
       getSupportActionBar().setDisplayShowHomeEnabled(true);

    }


    public void  llenarformulario2(Mujer mujer){


        txt_nombres.setText(mujer.getNombres());
//        txt_nombres.setEnabled(tr);

        txt_apellidos.setText(mujer.getApellidopaterno() +" "+mujer.getApellidomaterno());
//        txt_apellidos.setEnabled(false);
        txt_edad.setText(String.valueOf(mujer.Edad()));
//        txt_edad.setEnabled(true);

    }

    public void limpiarVista(){

        mujer=null;
        txt_nombres.setText("");
//        txt_nombres.setEnabled(true);
//        txt_apellidos.setEnabled(true);
//        txt_edad.setEnabled(true);
//        txt_nombres.requestFocus();
        txt_apellidos.setText("");
        txt_edad.setText("");

        btn_limpiar.setVisibility(View.GONE);

    }


}
